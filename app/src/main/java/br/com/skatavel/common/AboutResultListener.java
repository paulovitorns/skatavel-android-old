package br.com.skatavel.common;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface AboutResultListener extends BaseCommonListener {

    void onSuccess(String version, String date);
}
