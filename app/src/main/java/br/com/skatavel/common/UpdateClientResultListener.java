package br.com.skatavel.common;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface UpdateClientResultListener extends BaseCommonListener {
    void onSuccess();
}
