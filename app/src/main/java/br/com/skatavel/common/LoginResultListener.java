package br.com.skatavel.common;

import br.com.skatavel.model.Client;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface LoginResultListener extends BaseCommonListener {
    void onSuccess(Client client);
}
