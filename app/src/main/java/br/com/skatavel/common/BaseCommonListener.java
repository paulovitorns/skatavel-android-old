package br.com.skatavel.common;

import br.com.skatavel.model.ApiResponse;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface BaseCommonListener {

    void onError(ApiResponse error);

    void onConnectionFail(ApiResponse error);

    void onServerNotRespond(ApiResponse error);
}
