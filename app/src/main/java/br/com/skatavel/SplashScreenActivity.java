package br.com.skatavel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import br.com.skatavel.ui.view.activity.DashBoardActivity;
import br.com.skatavel.ui.view.activity.LoginActivity;
import br.com.skatavel.ui.view.activity.RegisterActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class SplashScreenActivity extends AppCompatActivity {

    @Bind(R.id.containerMask)
    RelativeLayout containerMaks;

    @Bind(R.id.imgLogo)
    ImageView imageView;

    private long TIME_BEFORE_LOAD_APP = 1300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        ButterKnife.bind(this);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        loadAnimation();
        loadApp();
    }

    private void loadAnimation(){
        Animation logoAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_long);
        imageView.startAnimation(logoAnimation);
    }

    private void loadApp(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(TIME_BEFORE_LOAD_APP);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashScreenActivity.this, DashBoardActivity.class));
                        overridePendingTransition(R.anim.fade_in_long, R.anim.fade_out_short);
                    }
                });
            }
        }).start();

    }


}
