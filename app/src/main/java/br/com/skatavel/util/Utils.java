package br.com.skatavel.util;

import android.util.TypedValue;

import br.com.skatavel.Skatavel;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class Utils {

    public static int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Skatavel.getContext().getResources().getDisplayMetrics());
    }
}
