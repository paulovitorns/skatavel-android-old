package br.com.skatavel.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import br.com.skatavel.model.Client;
import br.com.skatavel.model.Session;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class SharedPreferencesUtils {

    private static final String KEY             = Skatavel.getContext().getString(R.string.shared_session);
    private static final String SESSION_DATA    = Skatavel.getContext().getString(R.string.shared_session_data);

    private static SharedPreferences getPreferences(){
        SharedPreferences sharedPreferences = Skatavel.getContext().getSharedPreferences(KEY, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public static void saveSessionData(Session session){

        SharedPreferences preferences   = getPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        String dataPersistence          = new Gson().toJson(session, Session.class);

        editor.putString(SESSION_DATA, dataPersistence).commit();
    }

    public static void unsetSessionData(){
        SharedPreferences sharedPreferences = getPreferences();
        SharedPreferences.Editor editor     = sharedPreferences.edit();

        editor.putString(SESSION_DATA, null).commit();
    }

    public static Session getSessionData(){
        SharedPreferences sharedPreferences = getPreferences();
        String sessionStr                   = sharedPreferences.getString(SESSION_DATA, "");
        return new Gson().fromJson(sessionStr, Session.class);
    }


}
