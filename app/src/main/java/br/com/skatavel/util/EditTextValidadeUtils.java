package br.com.skatavel.util;

import android.content.Context;
import android.os.Build;
import android.widget.EditText;

import br.com.skatavel.R;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class EditTextValidadeUtils {


    static public void setErrorToView(EditText edt, Context context) {
        edt.setBackgroundResource(R.drawable.view_edittext_error_background);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            edt.setTextColor(context.getColor(R.color.red_error_text));
            edt.setHintTextColor(context.getColor(R.color.red_error_text));
        }else{
            edt.setTextColor(context.getResources().getColor(R.color.red_error_text));
            edt.setHintTextColor(context.getResources().getColor(R.color.red_error_text));
        }
    }

    static public void setNormalStateLoginToView(EditText edt, Context context) {
        edt.setBackgroundResource(R.drawable.view_edittext_background);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            edt.setTextColor(context.getColor(R.color.white));
            edt.setHintTextColor(context.getColor(R.color.white));
        }else{
            edt.setTextColor(context.getResources().getColor(R.color.white));
            edt.setHintTextColor(context.getResources().getColor(R.color.white));
        }
    }

    static public void setNormalStateToView(EditText edt, Context context) {
        edt.setBackgroundResource(R.drawable.view_edittext_background);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            edt.setTextColor(context.getColor(R.color.grey));
            edt.setHintTextColor(context.getColor(R.color.grey));
        }else{
            edt.setTextColor(context.getResources().getColor(R.color.grey));
            edt.setHintTextColor(context.getResources().getColor(R.color.grey));
        }
    }
}
