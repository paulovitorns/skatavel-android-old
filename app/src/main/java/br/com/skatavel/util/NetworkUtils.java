package br.com.skatavel.util;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import java.io.IOException;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import br.com.skatavel.model.ApiDialogTypes;
import br.com.skatavel.model.ApiResponse;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class NetworkUtils {

    public static boolean hasActiveInternetConnection() {
        return isNetworkAvailable();
    }

    private static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) Skatavel.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static boolean isLocationAvailable(){
        LocationManager lm = (LocationManager) Skatavel.getContext().getSystemService(Context.LOCATION_SERVICE);

        try {
            return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            return false;
        }
    }

    public static void showSnackDialog(View view, final Context context, final ApiDialogTypes dialogTypes){

        final Snackbar snackbar;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            snackbar = Snackbar
                    .make(view, dialogTypes.getMsg(), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(context.getColor(R.color.orange))
                    .setAction(dialogTypes.getTitle(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(dialogTypes.getIntentExtras());
                        }
                    });
        }else{
            snackbar = Snackbar
                    .make(view, dialogTypes.getMsg(), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(context.getResources().getColor(R.color.orange))
                    .setAction(dialogTypes.getTitle(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(dialogTypes.getIntentExtras());
                        }
                    });
        }

        snackbar.show();

    }



}
