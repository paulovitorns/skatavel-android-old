package br.com.skatavel;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class Skatavel extends MultiDexApplication {

    private static Context context;
    public static final String LAT_PARAM = "lat_user";
    public static final String LNG_PARAM = "lng_user";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
//        FacebookSdk.sdkInitialize(context);

//        Picasso.Builder builder = new Picasso.Builder(this);
//        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
//
//        Picasso built = builder.build();
//        built.setIndicatorsEnabled(false);
//        built.setLoggingEnabled(true);
//        Picasso.setSingletonInstance(built);

    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        Skatavel.context = context;
    }
}
