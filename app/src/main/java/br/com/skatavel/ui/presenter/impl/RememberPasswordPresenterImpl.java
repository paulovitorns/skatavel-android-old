package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.ui.presenter.RememberPasswordPresenter;
import br.com.skatavel.ui.view.RememberPasswordView;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class RememberPasswordPresenterImpl implements RememberPasswordPresenter {

    private RememberPasswordView view;

    public RememberPasswordPresenterImpl(RememberPasswordView view) {
        this.view   = view;

        init();
    }

    @Override
    public void init() {

    }

    @Override
    public void tryAgain() {

    }

    @Override
    public void goBack() {
        this.view.onBackPressed();
    }

    @Override
    public void validateAccessData(String email) {

        if(!email.isEmpty()){

            if(isValidEmail(email)){
                view.setDefaultEmailState();

            }else{
                view.setEmailFormatError();
            }

        }else{
            if(email.isEmpty()){
                view.setEmailEmptyError();
            }else {
                view.setDefaultEmailState();
            }

        }
    }

    @Override
    public boolean isValidEmail(String email) {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return false;
        }
        return true;
    }

}
