package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.business.service.SessionManagerService;
import br.com.skatavel.business.service.impl.SessionManagerServiceImpl;
import br.com.skatavel.model.ProfileMenu;
import br.com.skatavel.model.Session;
import br.com.skatavel.ui.adapter.ProfileMenuAdapter;
import br.com.skatavel.ui.presenter.ProfilePresenter;
import br.com.skatavel.ui.view.ProfileView;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ProfilePresenterImpl implements ProfilePresenter {

    private ProfileView     view;
    private SessionManagerService sessionManagerService;

    public ProfilePresenterImpl(ProfileView view) {
        this.view = view;
        init();
    }

    @Override
    public void init() {
        view.showLoading();
        sessionManagerService = new SessionManagerServiceImpl();
    }

    @Override
    public void tryAgain() {

    }

    @Override
    public void goBack() {

    }

    @Override
    public void lookForActiveSession() {
        Session session = sessionManagerService.getCurrentSession();

        if(session == null){
            view.setTitle("visitante");
            view.showProfileMenus(new ProfileMenuAdapter(ProfileMenu.getPublicItens(), view));
        } else {
            view.setTitle(session.getClient().getName());
            view.showProfileMenus(new ProfileMenuAdapter(ProfileMenu.getPrivateItens(), view));
        }
        view.hideLoading();
    }

    @Override
    public void disconnectClient() {
        sessionManagerService.destroyCurrentSession();

        view.showLoading();
        view.logOut();
    }

}
