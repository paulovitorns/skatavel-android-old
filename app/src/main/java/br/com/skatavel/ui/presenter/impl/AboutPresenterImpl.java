package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.R;
import br.com.skatavel.business.service.AboutService;
import br.com.skatavel.business.service.impl.AboutServiceImpl;
import br.com.skatavel.common.AboutResultListener;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.ui.presenter.AboutPresenter;
import br.com.skatavel.ui.view.AboutView;
import br.com.skatavel.util.StringUtils;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class AboutPresenterImpl implements AboutPresenter, AboutResultListener {

    private AboutView view;
    private AboutService service;

    public AboutPresenterImpl(AboutView view) {
        this.view = view;
        init();
    }

    @Override
    public void init() {
        view.showLoading();
        service = new AboutServiceImpl();
        service.getConfigInfos(this);
    }

    @Override
    public void tryAgain() {

    }

    @Override
    public void goBack() {
        this.view.onBackPressed();
    }

    @Override
    public void setCurrentYear(String date) {
        String dateInfo = StringUtils.setValue(view.getContext().getString(R.string.about_rights), date);
        this.view.showCurrentYear(dateInfo);
    }

    @Override
    public void setCurrentVersionCode(String version) {
        this.view.showVersionCode(version);
    }

    @Override
    public void onSuccess(String version, String date) {
        setCurrentVersionCode(version);
        setCurrentYear(date);
        view.hideLoading();
    }

    @Override
    public void onError(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onConnectionFail(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onServerNotRespond(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }
}
