package br.com.skatavel.ui.presenter;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface ProfilePresenter extends BasePresenter {

    void lookForActiveSession();

    void disconnectClient();

}
