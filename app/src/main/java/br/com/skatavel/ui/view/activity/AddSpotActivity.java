package br.com.skatavel.ui.view.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import br.com.skatavel.R;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.ui.presenter.AddSpotPresenter;
import br.com.skatavel.ui.presenter.impl.AddSpotPresenterImpl;
import br.com.skatavel.ui.view.AddSpotView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class AddSpotActivity extends BaseActivity implements AddSpotView,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraIdleListener{

    private AddSpotPresenter    presenter;
    private GoogleApiClient     mGoogleApiClient;
    private GoogleMap           gMap;
    private Location            myLocation;

    private static final int ZOOM_MAPS = 19;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_spot);

        ButterKnife.bind(this);

        setupNavigateModalActionBarWhite(getString(R.string.title_add_spot));

        presenter = new AddSpotPresenterImpl(this);

        if(savedInstanceState != null){
            myLocation = savedInstanceState.getParcelable("location");
        }

        SupportMapFragment mapFragment = SupportMapFragment.newInstance();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mapFrag, mapFragment).commit();
        mapFragment.getMapAsync(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("location", myLocation);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_modal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_close){
            finish();
            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_top);
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }

    }

    @OnClick(R.id.fabGoToLocation)
    @Override
    public void tapOnRequestLocale() {
        presenter.requestUserLocation();
    }

    @Override
    public void drawMap(double lat, double lng) {
        myLocation = new Location("gap location");
        myLocation.setLatitude(lat);
        myLocation.setLongitude(lng);

        CameraUpdate updateCam = CameraUpdateFactory.newLatLngZoom(
                new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                ZOOM_MAPS);

        gMap.animateCamera(updateCam);
    }

    @OnClick(R.id.nextStep)
    @Override
    public void tapOnNextStep() {
        presenter.sendDataToNextStep(myLocation);
    }

    @Override
    public void sendToNextStep(Intent intent) {
        startActivity(intent);
    }

    @Override
    public GoogleApiClient getGmapsClient() {
        return mGoogleApiClient;
    }

    @Override
    public void showDialogError(ApiResponse error) {
        super.showDialogError(error, this, this.presenter);
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(myLocation == null)
            presenter.processIntent(getIntent());
        else
            drawMap(myLocation.getLatitude(), myLocation.getLongitude());
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraIdle() {
        if(presenter.isUserHasDrag()) {
            LatLng latLng = gMap.getCameraPosition().target;
            myLocation.setLatitude(latLng.latitude);
            myLocation.setLongitude(latLng.longitude);
        }
    }

    @Override
    public void onCameraMoveStarted(int i) {
        if(i == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE){
            presenter.userHasDrag();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gMap = googleMap;

        // Hide the zoom controls as the button panel will cover it.
        this.gMap.getUiSettings().setZoomControlsEnabled(false);

        this.gMap.setOnCameraMoveStartedListener(this);
        this.gMap.setOnCameraIdleListener(this);

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = this.gMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.gmaps_style));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }

    }

}
