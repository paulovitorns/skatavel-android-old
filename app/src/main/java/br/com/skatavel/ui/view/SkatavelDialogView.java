package br.com.skatavel.ui.view;

import android.content.Intent;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface SkatavelDialogView {
    void create();
    void show();
    void setTitle(String title);
    void setMsg(String msg);
    void showRetryButton();
    void showResetPasswordButton();
    void navigateToActivity(Intent intent);
}
