package br.com.skatavel.ui.presenter;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface BaseMapsPresenter extends BasePresenter {

    void checkIfMapsPermissionsIsGranted();

    void startLocationUpdates();

    void getLocationAfterPermissionGranted();

    void getMyLocation();

    boolean hasSessionActive();
}
