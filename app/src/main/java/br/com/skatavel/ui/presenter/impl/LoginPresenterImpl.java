package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.business.api.vo.request.ClientRequest;
import br.com.skatavel.business.service.LoginService;
import br.com.skatavel.business.service.SessionManagerService;
import br.com.skatavel.business.service.impl.LoginServiceImpl;
import br.com.skatavel.business.service.impl.SessionManagerServiceImpl;
import br.com.skatavel.common.LoginResultListener;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.Client;
import br.com.skatavel.ui.presenter.LoginPresenter;
import br.com.skatavel.ui.view.LoginView;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class LoginPresenterImpl implements LoginPresenter, LoginResultListener {

    private Client          client;
    private LoginView       view;
    private LoginService    service;
    private SessionManagerService sessionManagerService;

    public LoginPresenterImpl(LoginView view) {
        this.view   = view;
        init();
    }

    @Override
    public void init() {
        this.service                = new LoginServiceImpl();
        this.sessionManagerService  = new SessionManagerServiceImpl();
    }

    @Override
    public void tryAgain() {
        validateAccessData(client);
    }

    @Override
    public void goBack() {
        this.view.onBackPressed();
    }

    @Override
    public void validateAccessData(Client client) {

        view.showLoading();

        this.client = client;
        if(!this.client.getEmail().isEmpty() && !this.client.getPassword().isEmpty()){

            if(isShortPass()){
                view.setPasswordShortError();
                view.hideLoading();
            }else if(!isValidEmail()){
                view.setErrorFormatLoginMessage();
                view.hideLoading();
            }else{
                view.setNormalPasswordState();
                view.setNormalLoginState();

                ClientRequest clientRequest = new ClientRequest();
                clientRequest.username = client.getEmail();
                clientRequest.password = client.getPassword();

                service.loginClient(clientRequest, this);
            }

        }else{
            if(this.client.getEmail().isEmpty()){
                view.setErrorEmptyLoginMessage();
            }else{
                view.setNormalLoginState();
            }

            if(this.client.getPassword().isEmpty()){
                view.setErrorEmptyPasswordMessage();
            }else{
                view.setNormalPasswordState();
            }

            view.hideLoading();
        }
    }

    @Override
    public boolean isShortPass() {
        if(client.getPassword().length() < 4){
            return true;
        }
        return false;
    }

    @Override
    public boolean isValidEmail() {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(client.getEmail()).matches()){
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess(Client client) {

        sessionManagerService.createNewSession(client);

        view.navigateToDashBoard();
        view.hideLoading();
    }

    @Override
    public void onError(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onConnectionFail(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onServerNotRespond(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }
}
