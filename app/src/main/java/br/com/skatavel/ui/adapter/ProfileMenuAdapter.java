package br.com.skatavel.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.skatavel.R;
import br.com.skatavel.model.ProfileMenu;
import br.com.skatavel.ui.view.ProfileView;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ProfileMenuAdapter extends BaseAdapter{

    private List<ProfileMenu>   menuList;
    private Activity            context;
    private ProfileView         profileView;

    public ProfileMenuAdapter(List<ProfileMenu> menuList, ProfileView view) {
        this.menuList       = menuList;
        this.profileView    = view;
        this.context        = view.getContext();
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int i) {

        return menuList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final ViewHolder holder = new ViewHolder();
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ProfileMenu menu = menuList.get(i);
        menu.setNavigation(context, menu);

        if(!menu.isDefaultFontStyle())
            view = inflater.inflate(R.layout.profile_bold_row, viewGroup, false);
        else
            view = inflater.inflate(R.layout.profile_row, viewGroup, false);

        holder.txItem           = (TextView) view.findViewById(R.id.txMenu);
        holder.containerItem    = (LinearLayout) view.findViewById(R.id.containerItem);

        holder.txItem.setText(menu.getMenu());

        holder.containerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!menu.isLogOutFunction && !menu.isShareFunction){
                    menu.navigation(context);
                }

                if(menu.isLogOutFunction){
                    profileView.onClickLogout();
                }

                if(menu.isShareFunction){
                    profileView.shareApp();
                }
            }
        });

        return view;
    }


    static class ViewHolder{
        TextView        txItem;
        LinearLayout    containerItem;
    }

}
