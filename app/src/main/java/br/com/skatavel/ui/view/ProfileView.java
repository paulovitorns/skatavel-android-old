package br.com.skatavel.ui.view;

import android.app.Activity;

import br.com.skatavel.ui.adapter.ProfileMenuAdapter;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface ProfileView extends BaseFragmentView {

    void setTitle(String title);

    void showProfileMenus(ProfileMenuAdapter adapter);

    void shareApp();

    void onClickLogout();

    void logOut();

    Activity getContext();
}
