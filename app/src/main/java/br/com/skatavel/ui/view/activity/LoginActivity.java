package br.com.skatavel.ui.view.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;

import br.com.skatavel.R;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.Client;
import br.com.skatavel.ui.presenter.LoginPresenter;
import br.com.skatavel.ui.presenter.impl.LoginPresenterImpl;
import br.com.skatavel.ui.view.LoginView;
import br.com.skatavel.util.EditTextValidadeUtils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class LoginActivity extends BaseActivity implements LoginView {

    @Bind(R.id.edtLayoutEmail)      TextInputLayout edtLayoutEmail;
    @Bind(R.id.edtLayoutPass)       TextInputLayout edtLayoutPass;

    @Bind(R.id.edtLogin)            EditText    edtLogin;
    @Bind(R.id.edtSenha)            EditText    edtSenha;

    @Bind(R.id.btnLogin)            Button      btnLogin;
    @Bind(R.id.btnLoginFacebook)    Button      btnLoginFacebook;

    private Client client;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        this.client = new Client();
        this.presenter = new LoginPresenterImpl(this);

        setupNavigateActionBar("");

        edtLayoutPass.setTypeface(Typeface.DEFAULT_BOLD);
        edtSenha.setTypeface(Typeface.DEFAULT_BOLD);
    }

    @OnClick(R.id.btnLogin)
    @Override
    public void onClickLogin() {

        client.setEmail(edtLogin.getText().toString());
        client.setPassword(edtSenha.getText().toString());

        presenter.validateAccessData(client);
    }

    @Override
    public void onClickLogWithFacebook() {

    }

    @Override
    public void setNormalLoginState() {
        EditTextValidadeUtils.setNormalStateLoginToView(edtLogin, getApplicationContext());
    }

    @Override
    public void setNormalPasswordState() {
        EditTextValidadeUtils.setNormalStateLoginToView(edtSenha, getApplicationContext());
    }

    @Override
    public void setErrorFormatLoginMessage() {
        EditTextValidadeUtils.setErrorToView(edtLogin, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(true);
        edtLayoutEmail.setError(getString(R.string.error_format_email));
    }

    @Override
    public void setErrorEmptyLoginMessage() {
        EditTextValidadeUtils.setErrorToView(edtLogin, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(true);
        edtLayoutEmail.setError(getString(R.string.error_empty_email));
    }

    @Override
    public void setErrorEmptyPasswordMessage() {
        EditTextValidadeUtils.setErrorToView(edtSenha, getApplicationContext());
        edtLayoutPass.setErrorEnabled(true);
        edtLayoutPass.setError(getString(R.string.error_empty_pass));
    }

    @Override
    public void setPasswordShortError() {
        EditTextValidadeUtils.setErrorToView(edtSenha, getApplicationContext());
        edtLayoutPass.setErrorEnabled(true);
        edtLayoutPass.setError(getString(R.string.error_short_pass));
    }

    @Override
    @OnClick(R.id.containerRecuperarDados)
    public void navigateToRecuperarSenha() {
        startActivity(new Intent(this, RememberPasswordActivity.class));
    }

    @Override
    public void navigateToDashBoard() {
        onBackPressed();
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showDialogError(ApiResponse error) {
        super.showDialogError(error, this, this.presenter);
    }
}
