package br.com.skatavel.ui.presenter.impl;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.LocationServices;

import br.com.skatavel.Skatavel;
import br.com.skatavel.ui.presenter.AddSpotPresenter;
import br.com.skatavel.ui.view.AddSpotView;
import br.com.skatavel.ui.view.activity.InfoNewSpotActivity;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class AddSpotPresenterImpl implements AddSpotPresenter {

    private double      lat;
    private double      lng;
    private AddSpotView view;
    private boolean     userHasDrag;

    public AddSpotPresenterImpl(AddSpotView view) {
        this.view = view;
        init();
    }

    @Override
    public void init() {
        view.showLoading();
    }

    @Override
    public void tryAgain() {}

    @Override
    public void goBack() {
        view.onBackPressed();
    }

    @Override
    public void processIntent(Intent intent) {
        lat = intent.getDoubleExtra(Skatavel.LAT_PARAM, 0);
        lng = intent.getDoubleExtra(Skatavel.LNG_PARAM, 0);
        generateMap();
    }

    @Override
    public void generateMap() {
        view.drawMap(lat, lng);
        view.hideLoading();
    }

    @Override
    public void generateMap(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
        this.userHasDrag = false;
        generateMap();
    }

    @Override
    public void requestUserLocation() {
        LocationManager manager = (LocationManager) view.getContext().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Request Permission
            return;
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(view.getGmapsClient());

        if(location == null)
            location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if(location == null)
            location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


        if(location != null){
            lat = location.getLatitude();
            lng = location.getLongitude();
        }

        generateMap();

    }

    @Override
    public void userHasDrag() {
        userHasDrag = true;
    }

    @Override
    public boolean isUserHasDrag() {
        return userHasDrag;
    }

    @Override
    public void sendDataToNextStep(Location location) {
        Intent intent = new Intent(view.getContext(), InfoNewSpotActivity.class);
        intent.putExtra(Skatavel.LAT_PARAM, location.getLatitude());
        intent.putExtra(Skatavel.LNG_PARAM, location.getLongitude());

        view.sendToNextStep(intent);
    }
}
