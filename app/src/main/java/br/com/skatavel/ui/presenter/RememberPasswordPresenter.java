package br.com.skatavel.ui.presenter;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface RememberPasswordPresenter extends BasePresenter {

    void validateAccessData(String email);

    boolean isValidEmail(String email);

}
