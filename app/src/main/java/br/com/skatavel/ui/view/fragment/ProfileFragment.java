package br.com.skatavel.ui.view.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import br.com.skatavel.R;
import br.com.skatavel.ui.adapter.ProfileMenuAdapter;
import br.com.skatavel.ui.presenter.ProfilePresenter;
import br.com.skatavel.ui.presenter.impl.ProfilePresenterImpl;
import br.com.skatavel.ui.view.DashBoardView;
import br.com.skatavel.ui.view.ProfileView;
import br.com.skatavel.ui.view.activity.DashBoardActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ProfileFragment extends Fragment implements ProfileView{

    private ProfilePresenter presenter;

    @Bind(R.id.listProfile)         ListView        listProfile;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ButterKnife.bind(this, view);

        presenter = new ProfilePresenterImpl(this);

        return view;
    }

    @Override
    public void onResume() {
        presenter.lookForActiveSession();
        super.onResume();
    }

    @Override
    public void setTitle(String title) {
        ((DashBoardView)getActivity()).setToolbarTitle("fala, "+title+"!");
    }

    @Override
    public void showProfileMenus(ProfileMenuAdapter adapter) {
        listProfile.setAdapter(adapter);
    }

    @Override
    public void shareApp() {

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getContext().getString(R.string.app_name));
            String sAux = getContext().getString(R.string.dialog_desc);
            sAux = sAux + getContext().getString(R.string.dialog_link);
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, getContext().getString(R.string.dialog_choose)));
        } catch(Exception e) {
            //e.toString();
        }
    }

    @Override
    public void onClickLogout() {
        presenter.disconnectClient();
    }

    @Override
    public void logOut() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(700);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Intent intent = new Intent(getActivity(), DashBoardActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
            }
        }).start();

    }

    @Override
    public Activity getContext() {
        return getActivity();
    }

    @Override
    public void showLoading() {
        ((DashBoardView)getActivity()).showLoading();
    }

    @Override
    public void hideLoading() {
        ((DashBoardView)getActivity()).hideLoading();
    }

}
