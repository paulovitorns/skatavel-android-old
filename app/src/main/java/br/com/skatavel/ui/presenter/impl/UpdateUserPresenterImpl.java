package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.business.api.vo.request.ClientRequest;
import br.com.skatavel.business.service.SessionManagerService;
import br.com.skatavel.business.service.UpdateUserService;
import br.com.skatavel.business.service.impl.SessionManagerServiceImpl;
import br.com.skatavel.business.service.impl.UpdateUserServiceImpl;
import br.com.skatavel.common.UpdateClientResultListener;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.Client;
import br.com.skatavel.ui.presenter.UpdateUserPresenter;
import br.com.skatavel.ui.view.UpdateUserView;
import br.com.skatavel.util.DateUtils;
import br.com.skatavel.util.SharedPreferencesUtils;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class UpdateUserPresenterImpl implements UpdateUserPresenter, UpdateClientResultListener {

    private Client                  client;
    private UpdateUserView          view;
    private UpdateUserService       service;
    private String                  confPass;
    private SessionManagerService   sessionManagerService;

    public UpdateUserPresenterImpl(UpdateUserView view){
        this.view = view;
        init();
    }

    @Override
    public void init() {
        view.showLoading();
        service                 = new UpdateUserServiceImpl();
        sessionManagerService   = new SessionManagerServiceImpl();
        view.populateInputs(SharedPreferencesUtils.getSessionData().getClient());
        view.hideLoading();
    }

    @Override
    public void tryAgain() {
        sendToUpdate(client, confPass);
    }

    @Override
    public void goBack() {
        this.view.onBackPressed();
    }

    @Override
    public void sendToUpdate(Client client, String confPass) {
        view.showLoading();
        this.client     = client;
        this.confPass   = confPass;

        if(validateUpdateDate(confPass)){

            ClientRequest request   = new ClientRequest();
            request.id              = client.getId();
            request.name            = client.getName();
            request.birthdate       = DateUtils.parseDateToQueryString(client.getBirthdate());
            request.gender          = client.getGender();
            request.username        = client.getEmail();
            request.password        = client.getPassword();

            service.updateClient(request, this);
        }else{
            view.hideLoading();
        }
    }

    @Override
    public boolean validateUpdateDate(String confPass) {

        if(this.client.getName().isEmpty()){
            view.setNameEmptyError();
            return false;
        }else{
            view.setNomeDefaultState();
        }

        if(this.client.getBirthdate() == null){
            view.setBirthdateEmptyError();
            return false;
        }else{
            view.setBirthdateDefaultState();
        }

        if(this.client.getGender().equalsIgnoreCase("undefined")){
            view.setGenderEmptyError();
            return false;
        }else{
            view.setGenderDefaultState();
        }

        if(this.client.getEmail().isEmpty()) {
            view.setEmailEmptyError();
            return false;
        }else if(!isValidEmail()){
            view.setEmailFormatError();
            return false;
        }else{
            view.setEmailDefaultState();
        }

        if(this.client.getPassword().isEmpty()){
            view.setPasswordEmptyError();
            return false;
        }else if(isShortPass()){
            view.setPasswordShortError();
            return false;
        }else{

            view.setPasswordDefaultState();

            if(confPass.isEmpty() || confPass.equalsIgnoreCase("")){
                view.setConfPasswordEmptyError();
                return false;
            }else{
                if(!isConfirmedPass(confPass)){
                    view.setConfPasswordNotEqualsToPassError();
                    return false;
                }else{
                    view.setPasswordDefaultState();
                    view.setConfPasswordDefaultState();
                }
            }

        }

        return true;
    }

    @Override
    public boolean isShortPass() {
        if(client.getPassword().length() < 4){
            return true;
        }
        return false;
    }

    @Override
    public boolean isConfirmedPass(String confPass) {
        if(client.getPassword().equalsIgnoreCase(confPass)){
            return true;
        }
        return false;
    }

    @Override
    public boolean isValidEmail() {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(client.getEmail()).matches()){
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        view.hideLoading();
        sessionManagerService.createNewSession(client);
        view.showSuccessDialog();
    }

    @Override
    public void onError(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onConnectionFail(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onServerNotRespond(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }
}
