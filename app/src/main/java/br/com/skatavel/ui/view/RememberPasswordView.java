package br.com.skatavel.ui.view;

import android.widget.EditText;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface RememberPasswordView extends BaseView {

    void onClickEnviar();

    void setEmailEmptyError();

    void setEmailFormatError();

    void setDefaultEmailState();

}
