package br.com.skatavel.ui.view.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import br.com.skatavel.R;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.ui.presenter.RememberPasswordPresenter;
import br.com.skatavel.ui.presenter.impl.RememberPasswordPresenterImpl;
import br.com.skatavel.ui.view.RememberPasswordView;
import br.com.skatavel.ui.view.component.EmailValidateWatcher;
import br.com.skatavel.util.EditTextValidadeUtils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class RememberPasswordActivity extends BaseActivity implements RememberPasswordView {

    @Bind(R.id.edtLayoutEmail)
    TextInputLayout edtLayoutEmail;
    @Bind(R.id.edtEmail)        EditText edtEmail;

    private RememberPasswordPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lembrar_senha);

        ButterKnife.bind(this);

        actionBar = getSupportActionBar();

        setupNavigateActionBar(getString(R.string.title_remember_password));

        presenter = new RememberPasswordPresenterImpl(this);

        edtEmail.addTextChangedListener(new EmailValidateWatcher(edtEmail, edtLayoutEmail, R.string.error_empty_email, getApplicationContext()));
    }

    @OnClick(R.id.btnEnviar)
    @Override
    public void onClickEnviar() {
        presenter.validateAccessData((edtEmail.getText().toString() == null) ? "": edtEmail.getText().toString());
    }

    @Override
    public void setEmailEmptyError() {
        EditTextValidadeUtils.setErrorToView(edtEmail, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(true);
        edtLayoutEmail.setError(getString(R.string.error_empty_email));
    }

    @Override
    public void setEmailFormatError() {
        EditTextValidadeUtils.setErrorToView(edtEmail, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(true);
        edtLayoutEmail.setError(getString(R.string.error_format_email));
    }

    @Override
    public void setDefaultEmailState() {
        EditTextValidadeUtils.setNormalStateToView(edtEmail, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(false);
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showDialogError(ApiResponse error) {
        super.showDialogError(error, this, this.presenter);
    }
}
