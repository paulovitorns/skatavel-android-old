package br.com.skatavel.ui.view.component;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

import android.content.Context;
import android.content.Intent;
import android.view.View;

import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.ui.presenter.BasePresenter;

public class CustomDialog extends SkatavelDialog{

    private ApiResponse     apiResponse;
    private BasePresenter   basePresenter;
    private Intent          intent;

    public CustomDialog(Context context, ApiResponse apiResponse, BasePresenter basePresenter){
        super(context);
        this.apiResponse    = apiResponse;
        this.basePresenter  = basePresenter;

        this.create();

    }

    public CustomDialog(Context context, ApiResponse apiResponse, BasePresenter basePresenter, Intent intent){
        super(context);
        this.apiResponse    = apiResponse;
        this.basePresenter  = basePresenter;
        this.intent         = intent;

        this.create();
    }

    @Override
    protected void create() {
        super.create();

        setTitle(apiResponse.getDialogType().getTitle());
        setMsg(apiResponse.getDialogType().getMsg());

        if(apiResponse.getDialogType().isShowTryAgainButton()){
            showRetryButton();
            btnTryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    basePresenter.tryAgain();
                }
            });
        }

        if(apiResponse.getDialogType().isShowResetPassButton()){
            showResetPasswordButton();
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(intent != null)
                    navigateToActivity(intent);
                if(apiResponse.getDialogType().isGobackFunction())
                    basePresenter.goBack();
            }
        });
    }

    @Override
    public void show() {
        super.show();
    }
}