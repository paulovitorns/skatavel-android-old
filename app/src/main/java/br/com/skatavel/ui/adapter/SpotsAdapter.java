package br.com.skatavel.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import br.com.skatavel.R;
import br.com.skatavel.model.ListOfSpots;
import br.com.skatavel.model.Spot;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class SpotsAdapter extends RecyclerView.Adapter<SpotsAdapter.ViewHolder> {

    private ListOfSpots spots;
    private Context     context;

    public SpotsAdapter(ListOfSpots spots, Context context) {
        this.spots      = spots;
        this.context    = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(context).inflate(R.layout.view_row_spot, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Spot spot = spots.getSpotList().get(position);

        if(spot != null){

            holder.title.setText(spot.getName());
            holder.info.setText(spot.getInfos());

            holder.btnFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.btnFav.setImageResource(R.drawable.ic_star_white_36dp);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return spots.getSpotList().size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @Bind(R.id.imgSpot) CircleImageView imgSpot;
        @Bind(R.id.txTitle) TextView        title;
        @Bind(R.id.txInfo)  TextView        info;
        @Bind(R.id.btnFav)  ImageButton     btnFav;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
