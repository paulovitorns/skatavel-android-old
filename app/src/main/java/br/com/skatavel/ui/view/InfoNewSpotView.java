package br.com.skatavel.ui.view;

import java.io.File;
import java.io.IOException;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface InfoNewSpotView extends BaseView {

    void tapOnFirstPhoto();

    void tapOnSencondPhoto();

    void tapOnThirdPhoto();

    void tapOnFourthPhoto();

    void openBottomSheet();

    void openCam();

    void openGalery();

    File createImageFile() throws IOException;

    void setPic();

    void tapOnSaveButton();

    void showSpotNameEmptyError();

    void showSpotNameDefaultState();

    void showSpotDescriptionEmptyError();

    void showDescSpotNameDefaultState();

}
