package br.com.skatavel.ui.view;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface AboutView extends BaseView {

    void showCurrentYear(String date);

    void showVersionCode(String version);

}
