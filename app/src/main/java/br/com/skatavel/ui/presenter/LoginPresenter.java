package br.com.skatavel.ui.presenter;

import br.com.skatavel.model.Client;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface LoginPresenter extends BasePresenter {

    void validateAccessData(Client client);

    boolean isShortPass();

    boolean isValidEmail();

}
