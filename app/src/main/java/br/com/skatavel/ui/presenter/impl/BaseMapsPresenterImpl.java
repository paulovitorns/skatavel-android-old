package br.com.skatavel.ui.presenter.impl;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import br.com.skatavel.business.service.SessionManagerService;
import br.com.skatavel.business.service.impl.SessionManagerServiceImpl;
import br.com.skatavel.model.ProfileMenu;
import br.com.skatavel.model.Session;
import br.com.skatavel.ui.adapter.ProfileMenuAdapter;
import br.com.skatavel.ui.presenter.BaseMapsPresenter;
import br.com.skatavel.ui.view.BaseMapsFragmentView;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class BaseMapsPresenterImpl implements BaseMapsPresenter, LocationListener {

    private BaseMapsFragmentView view;
    private boolean localePermissionCheck;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 1 min */
    private long FASTEST_INTERVAL = 3000; /* 30 sec */
    private SessionManagerService sessionManagerService;

    public BaseMapsPresenterImpl(BaseMapsFragmentView view) {
        this.view = view;
        init();
    }

    @Override
    public void init() {
        sessionManagerService = new SessionManagerServiceImpl();
        view.loadBottomSheet();
        this.checkIfMapsPermissionsIsGranted();
    }

    @Override
    public void tryAgain() {

    }

    @Override
    public void goBack() {
    }

    @Override
    public void checkIfMapsPermissionsIsGranted() {

        localePermissionCheck = ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (!localePermissionCheck) {
            view.requestLocationPermission();
        }

    }

    @Override
    public void startLocationUpdates() {

//        mLocationRequest = LocationRequest.create()
//                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setInterval(UPDATE_INTERVAL)
//                .setFastestInterval(FASTEST_INTERVAL);
//
//        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Request Permission
//            this.checkIfMapsPermissionsIsGranted();
//            return;
//        }
//        LocationServices.FusedLocationApi.requestLocationUpdates(view.getGmapsClient(), mLocationRequest, this);
//        Log.d("reque", "--->>>>");
    }

    @Override
    public void getLocationAfterPermissionGranted() {
        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Request Permission
            this.checkIfMapsPermissionsIsGranted();
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(view.getGmapsClient());
        view.updateLocation(location);
        startLocationUpdates();
    }

    @Override
    public void getMyLocation() {

        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Request Permission
            this.checkIfMapsPermissionsIsGranted();
            return;
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(view.getGmapsClient());
        view.updateLocation(location);

        if(mLocationRequest == null)
            startLocationUpdates();
    }

    @Override
    public boolean hasSessionActive() {
        Session session = sessionManagerService.getCurrentSession();

        if(session == null)
            return false;
        else
            return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        view.updateLocation(location);
    }
}
