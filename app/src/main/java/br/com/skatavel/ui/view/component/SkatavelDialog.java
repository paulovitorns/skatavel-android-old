package br.com.skatavel.ui.view.component;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import br.com.skatavel.R;
import br.com.skatavel.ui.view.activity.RememberPasswordActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

abstract class SkatavelDialog {

    @Bind(R.id.txtTitleDialog)  TextView    titleDialog;
    @Bind(R.id.txtTextDialog)   TextView    descDialog;
    @Bind(R.id.btnOk)           Button      btnOk;
    @Bind(R.id.btnTryAgain)     Button      btnTryAgain;
    @Bind(R.id.btnResetPass)    Button      btnResetPass;

    protected Dialog          dialog;
    protected Context         context;

    protected SkatavelDialog(Context context){
        this.context        = context;
    }

    protected void create(){
        dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fragment);
        dialog.setCancelable(true);

        ButterKnife.bind(this, dialog);
    }

    protected void show(){
        dialog.show();
    }

    protected void setTitle(String title){
        titleDialog.setText(title);
    }

    protected void setMsg(String msg){
        descDialog.setText(msg);
    }

    protected void showRetryButton(){
        btnTryAgain.setVisibility(View.VISIBLE);
    }

    protected void showResetPasswordButton(){
        btnResetPass.setVisibility(View.VISIBLE);
        btnResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(context, RememberPasswordActivity.class);
                context.startActivity(intent);
            }
        });
    }

    protected void navigateToActivity(Intent intent){
        context.startActivity(intent);
    }

}
