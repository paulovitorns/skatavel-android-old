package br.com.skatavel.ui.view;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface DashBoardView extends BaseView {

    void setSearchToolbarTitle(String gap, String location);

    void setToolbarTitle(String title);

    void setupBottomNavigationView();

    void loadDefaultFragment();

    void changeFragment(Fragment fragment);

    View getBottomNavigationView();

}
