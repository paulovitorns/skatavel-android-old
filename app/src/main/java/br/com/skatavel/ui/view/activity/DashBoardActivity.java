package br.com.skatavel.ui.view.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import br.com.skatavel.R;
import br.com.skatavel.model.ApiDialogTypes;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.DashboardNavigation;
import br.com.skatavel.ui.presenter.DashboardPresenter;
import br.com.skatavel.ui.presenter.impl.DashboardPresenterImpl;
import br.com.skatavel.ui.view.DashBoardView;
import br.com.skatavel.ui.view.fragment.MapSearchFragment;
import br.com.skatavel.util.NetworkUtils;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class DashBoardActivity extends BaseActivity implements DashBoardView {

    @Bind(R.id.bottomNavigation)    BottomNavigationView bottomNavigationView;

    private DashboardPresenter presenter;
    private boolean confirmedExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        ButterKnife.bind(this);
        presenter = new DashboardPresenterImpl(this);

    }

    @Override
    public void onBackPressed() {
        if(confirmedExit) {
            super.onBackPressed();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_exit_alert), Toast.LENGTH_SHORT).show();
            confirmedExit = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!NetworkUtils.hasActiveInternetConnection()){
            NetworkUtils.showSnackDialog(bottomNavigationView, this, ApiDialogTypes.NETWORK_DISABLED_SNACKBAR_DIALOG);
        }
    }

    @Override
    public void showLoading() { super.showLoading(); }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showDialogError(ApiResponse error) {
        super.showDialogError(error, this, this.presenter);
    }

    @Override
    public void setSearchToolbarTitle(String gap, String location) {
        String title;

        if(!location.equalsIgnoreCase(""))
            title = gap+". "+location;
        else
            title = gap;

        this.searchbarTitle.setText(title);
    }

    @Override
    public void setToolbarTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setupBottomNavigationView() {

        Menu bottomNavigationViewMenu = bottomNavigationView.getMenu();

        bottomNavigationViewMenu.findItem(R.id.navigator_profile).setChecked(false);

        bottomNavigationViewMenu.findItem(R.id.navigator_map).setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem selectedMenuItem) {

                switch (selectedMenuItem.getItemId()) {
                    case R.id.navigator_profile :
                        presenter.returnFragment(DashboardNavigation.PROFILE_NAV);
                        changeStyleToTransparentToolbar();
                        break;
                    case R.id.navigator_map :
                        presenter.returnFragment(DashboardNavigation.MAPS_NAV);
                        changeStyleToSearchToolbar();
                        break;
                    case R.id.navigator_favorite :
                        presenter.returnFragment(DashboardNavigation.FAVORITE_NAV);
                        changeStyleToSearchToolbarTransparent();
                        break;
                }

                return true;
            }
        });
    }

    @Override
    public void loadDefaultFragment() {

        MapSearchFragment fragment = MapSearchFragment.newInstance();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentView, fragment);
        transaction.commit();
    }

    @Override
    public void changeFragment(Fragment fragment) {

        if(confirmedExit)
            confirmedExit = false;

        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentView, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public View getBottomNavigationView() {
        return bottomNavigationView;
    }
}
