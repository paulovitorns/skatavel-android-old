package br.com.skatavel.ui.presenter;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface InfoNewSpotPresenter extends BasePresenter {
    void validateData(String name, String description, String[] photos);
    void requestAddData(String name, String description, String[] photos);
}
