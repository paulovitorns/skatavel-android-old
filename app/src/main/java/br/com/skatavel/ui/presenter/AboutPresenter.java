package br.com.skatavel.ui.presenter;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface AboutPresenter extends BasePresenter {

    void setCurrentYear(String date);

    void setCurrentVersionCode(String version);

}
