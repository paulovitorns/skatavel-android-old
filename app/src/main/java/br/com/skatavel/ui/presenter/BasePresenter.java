package br.com.skatavel.ui.presenter;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface BasePresenter {

    void init();

    void tryAgain();

    void goBack();
}
