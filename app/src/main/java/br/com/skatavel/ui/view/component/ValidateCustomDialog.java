package br.com.skatavel.ui.view.component;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

import android.content.Context;
import android.view.View;

public class ValidateCustomDialog extends SkatavelDialog{

    private String  title;
    private String  msg;

    public ValidateCustomDialog(Context context, String title, String msg){
        super(context);
        this.title  = title;
        this.msg    = msg;

        this.create();
    }

    @Override
    protected void create() {
        super.create();

        setTitle(title);
        setMsg(msg);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void show() {
        super.show();
    }
}