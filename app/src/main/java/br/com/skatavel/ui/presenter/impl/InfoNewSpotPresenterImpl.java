package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.ui.presenter.InfoNewSpotPresenter;
import br.com.skatavel.ui.view.InfoNewSpotView;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class InfoNewSpotPresenterImpl implements InfoNewSpotPresenter {

    private InfoNewSpotView view;

    public InfoNewSpotPresenterImpl(InfoNewSpotView view) {
        this.view = view;
        init();
    }

    @Override
    public void init() {

    }

    @Override
    public void tryAgain() {}

    @Override
    public void goBack() {}

    @Override
    public void validateData(String name, String description, String[] photos) {

        if(name == null && description == null && photos.length == 0){
            view.showSpotNameEmptyError();
            view.showSpotDescriptionEmptyError();
        }else{
            if(name == null){
                view.showSpotNameEmptyError();
            }else if(description == null){
                view.showSpotDescriptionEmptyError();
            }
//            else if(photos.length == 0){

//            }
            else{
                view.showSpotNameDefaultState();
                view.showDescSpotNameDefaultState();
                requestAddData(name, description, photos);
            }

        }

    }

    @Override
    public void requestAddData(String name, String description, String[] photos) {

    }
}
