package br.com.skatavel.ui.view;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface BaseFragmentView {

    void showLoading();

    void hideLoading();
}
