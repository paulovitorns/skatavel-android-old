package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.model.DashboardNavigation;
import br.com.skatavel.ui.presenter.DashboardPresenter;
import br.com.skatavel.ui.view.DashBoardView;
import br.com.skatavel.ui.view.fragment.MapSearchFragment;
import br.com.skatavel.ui.view.fragment.ProfileFragment;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class DashboardPresenterImpl implements DashboardPresenter {

    private DashBoardView view;
    private DashboardNavigation dashNav;

    public DashboardPresenterImpl(DashBoardView view) {
        this.view = view;
        init();
    }

    @Override
    public void init() {
        view.setupNavigateSearchActionBar();
        view.loadDefaultFragment();
        view.setupBottomNavigationView();
    }

    @Override
    public void tryAgain() {

    }

    @Override
    public void goBack() {
        this.view.onBackPressed();
    }

    @Override
    public boolean validateChangeFragment() {

        if(dashNav.getFragment() == null){
            return false;
        }
        return true;
    }

    @Override
    public void setFragmentToNavigator() {

        if(dashNav == DashboardNavigation.PROFILE_NAV){
            dashNav.setFragment(ProfileFragment.newInstance());
        }

        if(dashNav == DashboardNavigation.MAPS_NAV){
            dashNav.setFragment(MapSearchFragment.newInstance());
        }

        if(dashNav == DashboardNavigation.FAVORITE_NAV){
            //TODO: FAVORITE Fragment
            dashNav.setFragment(null);
        }

        returnFragment(dashNav);
    }

    @Override
    public void returnFragment(DashboardNavigation navigation) {
        this.dashNav = navigation;

        if(validateChangeFragment()){
            view.changeFragment(dashNav.getFragment());
        }else{
            setFragmentToNavigator();
        }

    }

}
