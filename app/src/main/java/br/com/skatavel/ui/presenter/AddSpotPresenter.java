package br.com.skatavel.ui.presenter;

import android.content.Intent;
import android.location.Location;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface AddSpotPresenter extends BasePresenter {

    void processIntent(Intent intent);

    void generateMap();

    void generateMap(double lat, double lng);

    void requestUserLocation();

    void userHasDrag();

    boolean isUserHasDrag();

    void sendDataToNextStep(Location location);
}
