package br.com.skatavel.ui.view;

import android.view.View;
import android.widget.EditText;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface LoginView extends BaseView {

    void onClickLogin();

    void onClickLogWithFacebook();

    void setNormalLoginState();

    void setNormalPasswordState();

    void setErrorFormatLoginMessage();

    void setErrorEmptyLoginMessage();

    void setErrorEmptyPasswordMessage();

    void setPasswordShortError();

    void navigateToRecuperarSenha();

    void navigateToDashBoard();

}
