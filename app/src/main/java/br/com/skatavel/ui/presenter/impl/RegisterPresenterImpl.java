package br.com.skatavel.ui.presenter.impl;

import br.com.skatavel.business.api.vo.request.ClientRequest;
import br.com.skatavel.business.service.RegisterUserService;
import br.com.skatavel.business.service.SessionManagerService;
import br.com.skatavel.business.service.impl.RegisterUserServiceImpl;
import br.com.skatavel.business.service.impl.SessionManagerServiceImpl;
import br.com.skatavel.common.RegisterResultListener;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.Client;
import br.com.skatavel.ui.presenter.RegisterPresenter;
import br.com.skatavel.ui.view.RegisterView;
import br.com.skatavel.util.DateUtils;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class RegisterPresenterImpl implements RegisterPresenter, RegisterResultListener {

    private Client                  client;
    private RegisterView            view;
    private RegisterUserService     service;
    private String                  confPass;
    private SessionManagerService   sessionManagerService;

    public RegisterPresenterImpl(RegisterView view){
        this.view = view;
        init();
    }

    @Override
    public void init() {
        this.service                = new RegisterUserServiceImpl();
        this.sessionManagerService  = new SessionManagerServiceImpl();
    }

    @Override
    public void tryAgain() {
        sendToRegister(client, confPass);
    }

    @Override
    public void goBack() {
        this.view.onBackPressed();
    }

    @Override
    public void sendToRegister(Client client, String confPass) {
        view.showLoading();
        this.client     = client;
        this.confPass   = confPass;

        if(validateRegisterDate(confPass)){

            ClientRequest request   = new ClientRequest();
            request.name            = client.getName();
            request.birthdate       = DateUtils.parseDateToQueryString(client.getBirthdate());
            request.gender          = client.getGender();
            request.username        = client.getEmail();
            request.password        = client.getPassword();

            service.registerClient(request, this);
        }else{
            view.hideLoading();
        }
    }

    @Override
    public boolean validateRegisterDate(String confPass) {

        if(this.client.getName().isEmpty()){
            view.setNameEmptyError();
            return false;
        }else{
            view.setNomeDefaultState();
        }

        if(this.client.getBirthdate() == null){
            view.setBirthdateEmptyError();
            return false;
        }else{
            view.setBirthdateDefaultState();
        }

        if(this.client.getGender().equalsIgnoreCase("undefined")){
            view.setGenderEmptyError();
            return false;
        }else{
            view.setGenderDefaultState();
        }

        if(this.client.getEmail().isEmpty()) {
            view.setEmailEmptyError();
            return false;
        }else if(!isValidEmail()){
            view.setEmailFormatError();
            return false;
        }else{
            view.setEmailDefaultState();
        }

        if(this.client.getPassword().isEmpty()){
            view.setPasswordEmptyError();
            return false;
        }else if(isShortPass()){
            view.setPasswordShortError();
            return false;
        }else{

            view.setPasswordDefaultState();

            if(confPass.isEmpty() || confPass.equalsIgnoreCase("")){
                view.setConfPasswordEmptyError();
                return false;
            }else{
                if(!isConfirmedPass(confPass)){
                    view.setConfPasswordNotEqualsToPassError();
                    return false;
                }else{
                    view.setPasswordDefaultState();
                    view.setConfPasswordDefaultState();
                }
            }

        }

        return true;
    }

    @Override
    public boolean isShortPass() {
        if(client.getPassword().length() < 4){
            return true;
        }
        return false;
    }

    @Override
    public boolean isConfirmedPass(String confPass) {
        if(client.getPassword().equalsIgnoreCase(confPass)){
            return true;
        }
        return false;
    }

    @Override
    public boolean isValidEmail() {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(client.getEmail()).matches()){
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess(Client client) {
        view.hideLoading();
        sessionManagerService.createNewSession(client);
        view.showSuccessDialog();
    }

    @Override
    public void onError(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onConnectionFail(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }

    @Override
    public void onServerNotRespond(ApiResponse error) {
        view.hideLoading();
        view.showDialogError(error);
    }
}
