package br.com.skatavel.ui.view;

import android.content.Intent;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface AddSpotView extends BaseView {

    void tapOnRequestLocale();

    void drawMap(double lat, double lng);

    void tapOnNextStep();

    void sendToNextStep(Intent intent);

    GoogleApiClient getGmapsClient();
}
