package br.com.skatavel.ui.view.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.io.File;
import java.io.IOException;

import br.com.skatavel.BuildConfig;
import br.com.skatavel.R;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.ui.presenter.InfoNewSpotPresenter;
import br.com.skatavel.ui.presenter.impl.InfoNewSpotPresenterImpl;
import br.com.skatavel.ui.view.InfoNewSpotView;
import br.com.skatavel.ui.view.component.PicBottomSheet;
import br.com.skatavel.util.EditTextValidadeUtils;
import br.com.skatavel.util.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class InfoNewSpotActivity extends BaseActivity implements InfoNewSpotView {

    @Bind(R.id.edtLayoutSpot)       TextInputLayout edtLayoutSpot;
    @Bind(R.id.containerDescSpot)   TextInputLayout edtLayoutDescSpot;

    @Bind(R.id.edtSpotName)         EditText        spotName;
    @Bind(R.id.edtDescSpot)         EditText        descSpot;

    @Bind(R.id.imgButtonFirst)      ImageButton     imgButtonFirst;
    @Bind(R.id.imgButtonSecond)     ImageButton     imgButtonSecond;
    @Bind(R.id.imgButtonThird)      ImageButton     imgButtonThird;
    @Bind(R.id.imgButtonFourth)     ImageButton     imgButtonFourth;

    private InfoNewSpotPresenter presenter;

    static final int    REQUEST_IMAGE_CAPTURE   = 1;
    static final int    REQUEST_RESULT_CODE     = -1;
    private static int  RESULT_LOAD_IMAGE       = 100;

    private String[] photosName = new String[4];
    private int currentItem;

    private int targetW;
    private int targetH;
    private int marginRight;

    private ImageButton imageButton;
    private BottomSheetDialogFragment bottomSheetDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_new_spot);

        ButterKnife.bind(this);

        bottomSheetDialogFragment = new PicBottomSheet();

        presenter = new InfoNewSpotPresenterImpl(this);

        marginRight = Utils.dp2px((int) (getResources().getDimension(R.dimen.margin_default) / getResources().getDisplayMetrics().density));

        setupNavigateActionBarWhite(getString(R.string.title_info_new_spot));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.imgButtonFirst)
    @Override
    public void tapOnFirstPhoto() {
        currentItem = 0;
        imageButton = imgButtonFirst;
        openBottomSheet();
    }

    @OnClick(R.id.imgButtonSecond)
    @Override
    public void tapOnSencondPhoto() {
        currentItem = 1;
        imageButton = imgButtonSecond;
        openBottomSheet();
    }

    @OnClick(R.id.imgButtonThird)
    @Override
    public void tapOnThirdPhoto() {
        currentItem = 2;
        imageButton = imgButtonThird;
        openBottomSheet();
    }

    @OnClick(R.id.imgButtonFourth)
    @Override
    public void tapOnFourthPhoto() {
        currentItem = 3;
        imageButton = imgButtonFourth;
        openBottomSheet();
    }

    @Override
    public void openBottomSheet() {
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void openCam() {

        bottomSheetDialogFragment.dismiss();

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        BuildConfig.APPLICATION_ID+".fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    public void openGalery() {

        bottomSheetDialogFragment.dismiss();

        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    @Override
    public File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = String.format("gap_image_%s", Integer.toString(currentItem));
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        photosName[currentItem] = image.getAbsolutePath();
        return image;
    }

    @Override
    public void setPic() {

        if(targetW == 0)
            targetW = imgButtonFirst.getMeasuredWidth();

        if(targetH == 0)
            targetH = imgButtonFirst.getMeasuredHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds    = true;

        // Determine how much to scale down the image
        int scaleFactor = 8;
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(photosName[currentItem], bmOptions);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(imageButton.getMeasuredWidth(), imageButton.getMeasuredHeight());

        if(!imageButton.equals(imgButtonFourth))
            params.setMargins(0,0,marginRight,0);

        imageButton.setLayoutParams(params);
        imageButton.setImageBitmap(bitmap);
    }

    @Override
    public void tapOnSaveButton() {
        presenter.requestAddData(spotName.getText().toString(), descSpot.getContext().toString(), photosName);
    }

    @Override
    public void showSpotNameEmptyError() {
        EditTextValidadeUtils.setErrorToView(spotName, getApplicationContext());
        edtLayoutSpot.setErrorEnabled(true);
        edtLayoutSpot.setError(getString(R.string.error_spot_empty));
    }

    @Override
    public void showSpotNameDefaultState() {
        EditTextValidadeUtils.setNormalStateToView(spotName, getApplicationContext());
        edtLayoutSpot.setErrorEnabled(false);
    }

    @Override
    public void showSpotDescriptionEmptyError() {
        EditTextValidadeUtils.setErrorToView(descSpot, getApplicationContext());
        edtLayoutDescSpot.setErrorEnabled(true);
        edtLayoutDescSpot.setError(getString(R.string.error_desc_spot_empty));
    }

    @Override
    public void showDescSpotNameDefaultState() {
        EditTextValidadeUtils.setNormalStateToView(descSpot, getApplicationContext());
        edtLayoutDescSpot.setErrorEnabled(false);
    }

    @Override
    public void showDialogError(ApiResponse error) {
        super.showDialogError(error, this, this.presenter);
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == REQUEST_RESULT_CODE) {
            setPic();
        } else if(requestCode == RESULT_LOAD_IMAGE && resultCode == REQUEST_RESULT_CODE && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            photosName[currentItem] = picturePath;
            setPic();
        }
    }

}
