package br.com.skatavel.ui.view;

import android.content.Intent;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface RegisterView extends BaseView {

    void onClickBtnSave();

    void setNameEmptyError();

    void setBirthdateEmptyError();

    void setGenderEmptyError();

    void setEmailEmptyError();

    void setEmailFormatError();

    void setPasswordEmptyError();

    void setConfPasswordEmptyError();

    void setPasswordShortError();

    void setConfPasswordNotEqualsToPassError();

    void setNomeDefaultState();

    void setBirthdateDefaultState();

    void setGenderDefaultState();

    void setEmailDefaultState();

    void setPasswordDefaultState();

    void setConfPasswordDefaultState();

    void showSuccessDialog();

}
