package br.com.skatavel.ui.view;

import android.content.Context;

import br.com.skatavel.model.ApiResponse;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface BaseView {

    void setupNavigateActionBar(String title);

    void setupNavigateActionBarWhite(String title);

    void setupNavigateModalActionBarWhite(String title);

    void setupNavigateSearchActionBar();

    void changeStyleToTransparentToolbar();

    void changeStyleToSearchToolbar();

    void changeStyleToSearchToolbarTransparent();

    void showLoading();

    void hideLoading();

    void showDialogError(ApiResponse error);

    Context getContext();

    void onBackPressed();

}
