package br.com.skatavel.ui.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import br.com.skatavel.R;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class SimpleSpinnerAdapter extends BaseAdapter {

    private Context ctx;
    private String[] lista;

    public SimpleSpinnerAdapter(Context context, String[] lista){
        this.ctx = context;
        this.lista = lista;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lista.length;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return lista[arg0];
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        LayoutInflater inflater = (LayoutInflater)   ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.view_spinner_textview, null);

        TextView txView = (TextView) view.findViewById(R.id.txView);

        txView.setText(this.lista[position]);

        if(position == 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                txView.setTextColor(ctx.getColor(R.color.aux_hint_common_color));
            }else{
                txView.setTextColor(ctx.getResources().getColor(R.color.aux_hint_common_color));
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                txView.setTextColor(ctx.getColor(R.color.edittext_common_color));
            }else{
                txView.setTextColor(ctx.getResources().getColor(R.color.edittext_common_color));
            }
        }

        return txView;

    }

}
