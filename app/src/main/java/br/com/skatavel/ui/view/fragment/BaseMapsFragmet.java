package br.com.skatavel.ui.view.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import br.com.skatavel.model.ApiDialogTypes;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.ListOfSpots;
import br.com.skatavel.model.Spot;
import br.com.skatavel.ui.presenter.BaseMapsPresenter;
import br.com.skatavel.ui.presenter.impl.BaseMapsPresenterImpl;
import br.com.skatavel.ui.view.BaseMapsFragmentView;
import br.com.skatavel.ui.view.DashBoardView;
import br.com.skatavel.ui.view.activity.AddSpotActivity;
import br.com.skatavel.ui.view.activity.DashBoardActivity;
import br.com.skatavel.util.NetworkUtils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class BaseMapsFragmet extends Fragment implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        BaseMapsFragmentView {

    @Bind(R.id.bottom_sheet)        NestedScrollView    bottomSheet;
    @Bind(R.id.cdShowBottomSheet)   CardView            cardShowBottomSheet;

    private View view;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap gMap;
    private Location myLocation;
    private LocationManager locationManager;

    private BaseMapsPresenter presenter;

    private Marker marker;

    private static final int PERMISSION_LOCATION_REQUEST_CODE = 200;

    private boolean firstLoad = true;

    private BottomSheetBehavior mBottomSheetBehavior;

    private ListOfSpots spots;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_view_map, container, false);
        setHasOptionsMenu(true);
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.mapFrag, mapFragment).commit();
        mapFragment.getMapAsync(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        ButterKnife.bind(this, view);

        presenter = new BaseMapsPresenterImpl(this);

        ((DashBoardView)getActivity()).setSearchToolbarTitle("Qualquer pico", "São Paulo - SP");

        spots = new ListOfSpots();

        Spot spot01 = new Spot(new LatLng(-23.634189, -46.640566), "Chopp & Cia", "500mts da sua distância");
        Spot spot02 = new Spot(new LatLng(-23.634110, -46.639866), "Teste 2", "500mts da sua distância");
        Spot spot03 = new Spot(new LatLng(-23.634368, -46.642020), "Teste 3", "500mts da sua distância");
        Spot spot04 = new Spot(new LatLng(-23.634154, -46.642355), "Teste 4", "500mts da sua distância");
        Spot spot05 = new Spot(new LatLng(-23.634115, -46.643026), "Teste 5", "500mts da sua distância");
        Spot spot06 = new Spot(new LatLng(-23.633815, -46.642229), "Teste 6", "500mts da sua distância");

        spots.addSpot(spot01);
        spots.addSpot(spot02);
        spots.addSpot(spot03);
        spots.addSpot(spot04);
        spots.addSpot(spot05);
        spots.addSpot(spot06);
        spots.addSpot(spot01);
        spots.addSpot(spot02);
        spots.addSpot(spot03);
        spots.addSpot(spot04);
        spots.addSpot(spot05);
        spots.addSpot(spot06);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search_bar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem searchIcon = menu.findItem(R.id.menu_search);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            searchIcon.getIcon().setColorFilter(getContext().getColor(R.color.orange), PorterDuff.Mode.SRC_ATOP);
        else
            searchIcon.getIcon().setColorFilter(getContext().getResources().getColor(R.color.orange), PorterDuff.Mode.SRC_ATOP);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!NetworkUtils.isLocationAvailable()){
            NetworkUtils.showSnackDialog(((DashBoardView)getActivity()).getBottomNavigationView(), getContext(), ApiDialogTypes.LOCATION_DISABLED_SNACKBAR_DIALOG);
        }else{
            onStart();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        presenter.startLocationUpdates();
        myLocation = requestMyLocation();

        if (myLocation != null) {
            drawMarker();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("CONNECTION_SUSPENDED", "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("CONNECTION_FAILED", "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = this.gMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.gmaps_style));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }
    }

    @Override
    public void loadBottomSheet() {

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        int sizeOfPeeker = (int) getResources().getDimension(R.dimen.bottom_sheet_size);

        mBottomSheetBehavior.setPeekHeight(sizeOfPeeker);
        mBottomSheetBehavior.setHideable(true);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                if(newState == BottomSheetBehavior.STATE_HIDDEN){
                    Animation bounceAnimator = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in);

                    bounceAnimator.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            cardShowBottomSheet.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                    cardShowBottomSheet.startAnimation(bounceAnimator);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    @OnClick(R.id.cdShowBottomSheet)
    @Override
    public void openBottomSheet(){

        CloseSpotsFragment spotsFragment = CloseSpotsFragment.newInstance(spots);

        FragmentTransaction trans = getChildFragmentManager().beginTransaction();
        trans.replace(R.id.fragmentView, spotsFragment).commit();

        bottomSheet.setVisibility(View.VISIBLE);

        Animation bounceAnimator = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out);

        bounceAnimator.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                cardShowBottomSheet.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        cardShowBottomSheet.startAnimation(bounceAnimator);

        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @OnClick(R.id.fabAddGap)
    @Override
    public void onClickFabAddSpot() {

        if(presenter.hasSessionActive()){
            Intent intent = new Intent(getActivity(), AddSpotActivity.class);
            intent.putExtra(Skatavel.LAT_PARAM, (myLocation != null) ? myLocation.getLatitude() : 0.0);
            intent.putExtra(Skatavel.LNG_PARAM, (myLocation != null) ? myLocation.getLongitude() : 0.0);
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_top);
        }else{
            ((DashBoardActivity)getActivity()).showDialogError(ApiResponse.getDefaultSessionNeeded());
        }
    }

    @OnClick(R.id.fabGoToLocation)
    @Override
    public void onClickFabLocale() {
        myLocation = requestMyLocation();
        if(myLocation != null)
            drawMarkerWithAnimation();
    }

    @Override
    public void requestLocationPermission() {
        requestPermissions(
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_LOCATION_REQUEST_CODE);
    }

    @Override
    public Location requestMyLocation() {
        LocationManager manager = (LocationManager) view.getContext().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return null;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(location == null)
            location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if(location == null)
            location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        return location;
    }

    @Override
    public void drawMarker() {

        if(marker != null){
            marker.remove();
        }

        LatLng onlyMarker;

        onlyMarker = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        MarkerOptions opt = new MarkerOptions();

        opt.position(onlyMarker);
        opt.title("minha localização");

        //add the marker and
        marker = gMap.addMarker(opt);

        CameraUpdate updateCam = CameraUpdateFactory.newLatLngZoom(onlyMarker, 17);
        gMap.moveCamera(updateCam);

        drawMarker(new LatLng(-23.4998071,-46.6468254));

    }

    @Override
    public void drawMarker(LatLng latLng) {

        MarkerOptions opt = new MarkerOptions();

        opt.position(latLng);

        opt.title("teste role");

        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_skate_marker));

        //add the marker and
        gMap.addMarker(opt);
//        marker = gMap.addMarker(opt);
    }

    @Override
    public void drawMarkerWithAnimation() {

        if(marker != null){
            marker.remove();
        }

        LatLng onlyMarker;

        onlyMarker = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        MarkerOptions opt = new MarkerOptions();

        opt.position(onlyMarker);
        opt.title("minha localização");

        //add the marker and
        marker = gMap.addMarker(opt);

        CameraUpdate updateCam = CameraUpdateFactory.newLatLngZoom(onlyMarker, 17);
        gMap.animateCamera(updateCam);

    }

    @Override
    public void updateLocation(Location location) {

        myLocation = location;

        if(myLocation != null){

            if(marker !=null) {
                marker.remove();
                drawMarker();
            }
        }
    }

    @Override
    public GoogleApiClient getGmapsClient() {
        return this.mGoogleApiClient;
    }

    @Override
    public void showLoading() {
        ((DashBoardView)getActivity()).showLoading();
    }

    @Override
    public void hideLoading() {
        ((DashBoardView)getActivity()).hideLoading();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_LOCATION_REQUEST_CODE: {

                if ( grantResults.length > 0
                        && ((grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        || (grantResults[1] == PackageManager.PERMISSION_GRANTED))) {

                    if(firstLoad){
                        myLocation = requestMyLocation();
                        if(myLocation != null)
                            drawMarker();
                    }
                    firstLoad = false;
                    presenter.getLocationAfterPermissionGranted();

                } else {

                }
                return;
            }

        }
    }
}
