package br.com.skatavel.ui.presenter;

import br.com.skatavel.model.Client;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface RegisterPresenter extends BasePresenter {

    void sendToRegister(Client client, String confPass);

    boolean validateRegisterDate(String confPass);

    boolean isShortPass();

    boolean isConfirmedPass(String confPass);

    boolean isValidEmail();

}
