package br.com.skatavel.ui.presenter;

import br.com.skatavel.model.Client;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface UpdateUserPresenter extends BasePresenter {

    void sendToUpdate(Client client, String confPass);

    boolean validateUpdateDate(String confPass);

    boolean isShortPass();

    boolean isConfirmedPass(String confPass);

    boolean isValidEmail();

}
