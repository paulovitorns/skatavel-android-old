package br.com.skatavel.ui.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.skatavel.R;
import br.com.skatavel.model.ListOfSpots;
import br.com.skatavel.ui.adapter.SpotsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class CloseSpotsFragment extends Fragment {

    @Bind(R.id.rvSpots) RecyclerView recycler;

    private ListOfSpots spots;

    public CloseSpotsFragment() {
        // Required empty public constructor
    }

    public static CloseSpotsFragment newInstance(ListOfSpots spots) {
        CloseSpotsFragment fragment = new CloseSpotsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ListOfSpots.KEY, spots);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_spots_dashboard, container, false);

        ButterKnife.bind(this, view);

        this.spots = (ListOfSpots) getArguments().getSerializable(ListOfSpots.KEY);

        if(spots == null){
            //TODO: Implements empty state
        }else{

            SpotsAdapter adapter = new SpotsAdapter(spots, getContext());

            recycler.setHasFixedSize(true);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recycler.setLayoutManager(mLayoutManager);

            recycler.setAdapter(adapter);

        }

        return view;
    }

}
