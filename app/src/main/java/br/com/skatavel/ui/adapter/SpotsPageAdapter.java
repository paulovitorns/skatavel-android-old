package br.com.skatavel.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import br.com.skatavel.model.SpotPage;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class SpotsPageAdapter extends FragmentPagerAdapter {

    List<SpotPage> fragmentsList;

    public SpotsPageAdapter(FragmentManager fm, List<SpotPage> fragmentsList) {
        super(fm);
        this.fragmentsList  = fragmentsList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentsList.get(position).getFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = (position == 0) ? Skatavel.getContext().getString(R.string.tab_near) : Skatavel.getContext().getString(R.string.tab_fav);
        return title;
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

}
