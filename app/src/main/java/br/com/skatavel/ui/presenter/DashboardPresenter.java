package br.com.skatavel.ui.presenter;

import br.com.skatavel.model.DashboardNavigation;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface DashboardPresenter extends BasePresenter {

    boolean validateChangeFragment();

    void setFragmentToNavigator();

    void returnFragment(DashboardNavigation navigation);

}
