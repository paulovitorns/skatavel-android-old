package br.com.skatavel.ui.view.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import br.com.skatavel.R;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.ui.presenter.BasePresenter;
import br.com.skatavel.ui.view.component.CustomDialog;
import br.com.skatavel.ui.view.component.ProgressDialog;
import butterknife.Bind;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @Bind(R.id.searchToolbar)   Toolbar     searchToolbar;
    @Nullable
    @Bind(R.id.toolbar)         Toolbar     toolbar;
    @Nullable
    @Bind(R.id.toolbar_title)   TextView    title;
    @Nullable
    @Bind(R.id.searchbar_title) TextView    searchbarTitle;

    public  ActionBar       actionBar;
    private ProgressDialog  mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setupNavigateActionBar(String title) {

        setSupportActionBar(toolbar);

        this.actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        this.title.setText(title);

    }

    public void setupNavigateActionBarWhite(String title) {

        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        this.actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                toolbar.getNavigationIcon().setColorFilter(getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            else
                toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        }

        this.title.setText(title);

    }


    public void setupNavigateModalActionBarWhite(String title) {

        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        this.actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }

        this.title.setText(title);
    }

    public void setupNavigateSearchActionBar() {

        setSupportActionBar(searchToolbar);

        this.actionBar = getSupportActionBar();
        if (actionBar != null) {
             actionBar.setDisplayShowTitleEnabled(false);

            if(toolbar != null)
                toolbar.setVisibility(View.GONE);

        }
    }

    public void changeStyleToTransparentToolbar() {

        if(searchToolbar != null && actionBar != null){

            title.setVisibility(View.VISIBLE);
            searchbarTitle.setVisibility(View.GONE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                searchToolbar.setBackgroundColor(getColor(android.R.color.transparent));
            }else{
                searchToolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            }
        }
    }

    public void changeStyleToSearchToolbar() {

        if(searchToolbar != null && actionBar != null){

            title.setVisibility(View.GONE);
            searchbarTitle.setVisibility(View.VISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                searchToolbar.setBackground(getDrawable(R.drawable.border_radius));
            }else{
                searchToolbar.setBackground(getResources().getDrawable(R.drawable.border_radius));
            }

        }
    }

    public void changeStyleToSearchToolbarTransparent() {

        if(searchToolbar != null && actionBar != null){

            title.setVisibility(View.GONE);
            searchbarTitle.setVisibility(View.VISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                searchToolbar.setBackgroundColor(getColor(android.R.color.transparent));
            }else{
                searchToolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            }

        }
    }

    public void showLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            hideLoading();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.show();
    }

    public void hideLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void showDialogError(ApiResponse error, Activity context, BasePresenter basePresenter){
        CustomDialog customDialog = new CustomDialog(context, error, basePresenter);
        customDialog.show();
    }

    public Context getContext(){
        return getApplicationContext();
    }

}
