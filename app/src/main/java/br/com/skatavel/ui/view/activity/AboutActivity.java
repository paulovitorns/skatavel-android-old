package br.com.skatavel.ui.view.activity;

import android.os.Bundle;
import android.widget.TextView;

import br.com.skatavel.R;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.ui.presenter.AboutPresenter;
import br.com.skatavel.ui.presenter.impl.AboutPresenterImpl;
import br.com.skatavel.ui.view.AboutView;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class AboutActivity extends BaseActivity implements AboutView {

    @Bind(R.id.txVersion)   TextView txVersion;
    @Bind(R.id.txRights)    TextView txRights;

    private AboutPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        ButterKnife.bind(this);

        presenter = new AboutPresenterImpl(this);

        setupNavigateActionBar("");

    }

    @Override
    public void showCurrentYear(String date) {
        txRights.setText(date);
    }

    @Override
    public void showVersionCode(String version) {
        txVersion.setText(version);
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showDialogError(ApiResponse error) {
        super.showDialogError(error, this, this.presenter);
    }
}
