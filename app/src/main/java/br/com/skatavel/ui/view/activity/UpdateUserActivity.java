package br.com.skatavel.ui.view.activity;

import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import br.com.skatavel.model.ApiDialogTypes;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.Client;
import br.com.skatavel.ui.adapter.SimpleSpinnerAdapter;
import br.com.skatavel.ui.presenter.UpdateUserPresenter;
import br.com.skatavel.ui.presenter.impl.UpdateUserPresenterImpl;
import br.com.skatavel.ui.view.UpdateUserView;
import br.com.skatavel.ui.view.component.CustomDialog;
import br.com.skatavel.ui.view.component.DateValidateWatcher;
import br.com.skatavel.ui.view.component.SimpleValidateWatcher;
import br.com.skatavel.util.DateUtils;
import br.com.skatavel.util.EditTextValidadeUtils;
import br.com.skatavel.util.StringUtils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class UpdateUserActivity extends BaseActivity implements UpdateUserView{

    @Bind(R.id.edtLayoutName)       TextInputLayout edtLayoutName;
    @Bind(R.id.edtLayoutBirth)      TextInputLayout edtLayoutBirth;
    @Bind(R.id.edtLayoutEmail)      TextInputLayout edtLayoutEmail;
    @Bind(R.id.edtLayoutPass)       TextInputLayout edtLayoutPass;
    @Bind(R.id.edtLayoutConfPass)   TextInputLayout edtLayoutConfPass;

    @Bind(R.id.edtName)             EditText edtName;
    @Bind(R.id.edtBirth)            EditText edtBirth;
    @Bind(R.id.spGender)            Spinner spGender;
    @Bind(R.id.edtEmail)            EditText edtEmail;
    @Bind(R.id.edtPass)             EditText edtPass;
    @Bind(R.id.edtConfPass)         EditText edtConfPass;

    @Bind(R.id.containerSpinner)    RelativeLayout containerSpinner;
    @Bind(R.id.imgSpinner)          ImageView imgSpinner;

    private UpdateUserPresenter presenter;
    private Client client;
    private String[] gender      = Skatavel.getContext().getResources().getStringArray(R.array.gender);
    private boolean isFirstChange = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);

        ButterKnife.bind(this);

        actionBar = getSupportActionBar();

        setupNavigateActionBar(getApplicationContext().getString(R.string.title_update_account));

        edtName.addTextChangedListener(new SimpleValidateWatcher(edtName, edtLayoutName, R.string.error_empty_name, getApplicationContext()));
        edtBirth.addTextChangedListener(new DateValidateWatcher(edtBirth, edtLayoutBirth, R.string.error_empty_birth, getApplicationContext()));

        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        spGender.setLayoutParams(lp1);
        spGender.setAdapter(new SimpleSpinnerAdapter(this, gender));

        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                TextView txView = (TextView) parent.getChildAt(0);

                if(!isFirstChange){

                    if(parent.getSelectedItemPosition() == 0){
                        txView.setText(getString(R.string.error_empty_gender));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            txView.setTextColor(getColor(R.color.red));
                        }else{
                            txView.setTextColor(getResources().getColor(R.color.red));
                        }

                        setGenderEmptyError();
                    }else{
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            txView.setTextColor(getColor(R.color.edittext_common_color));
                        }else{
                            txView.setTextColor(getResources().getColor(R.color.edittext_common_color));
                        }

                        setGenderDefaultState();
                    }
                }

                isFirstChange = false;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        presenter   = new UpdateUserPresenterImpl(this);

    }

    @Override
    public void populateInputs(Client client) {

        this.client = client;

        edtName.setText(client.getName());
        edtBirth.setText(DateUtils.parseDateToString(client.getBirthdate()));
        edtEmail.setText(client.getEmail());
        spGender.setSelection(((client.getGender().equals("m")) ? 1 : 2));

    }

    @OnClick(R.id.btnEnviar)
    @Override
    public void onClickBtnSave() {

        client.setName(edtName.getText().toString());

        if(edtBirth.getText().toString().isEmpty())
            client.setBirthdate(null);
        else
            client.setBirthdate(DateUtils.parseStringToDate(edtBirth.getText().toString()));

        if(spGender.getSelectedItemPosition() == 0){
            client.setGender("undefined");
        }else if(spGender.getSelectedItemPosition() == 1){
            client.setGender("m");
        }else {
            client.setGender("f");
        }

        client.setEmail(edtEmail.getText().toString());
        client.setPassword(edtPass.getText().toString());

        presenter.sendToUpdate(client, edtConfPass.getText().toString());
    }

    @Override
    public void setNameEmptyError() {
        EditTextValidadeUtils.setErrorToView(edtName, getApplicationContext());
        edtLayoutName.setErrorEnabled(true);
        edtLayoutName.setError(getString(R.string.error_empty_name));
    }

    @Override
    public void setBirthdateEmptyError() {
        EditTextValidadeUtils.setErrorToView(edtBirth, getApplicationContext());
        edtLayoutBirth.setErrorEnabled(true);
        edtLayoutBirth.setError(getString(R.string.error_empty_birth));
    }

    @Override
    public void setGenderEmptyError() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            imgSpinner.getDrawable().setColorFilter(getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        else
            imgSpinner.getDrawable().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            containerSpinner.setBackground(getDrawable(R.drawable.view_spinner_border_error));
        }else{
            containerSpinner.setBackground(getResources().getDrawable(R.drawable.view_spinner_border_error));
        }

        TextView txView = (TextView) spGender.getChildAt(0);
        txView.setText(getString(R.string.error_empty_gender));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            txView.setTextColor(getColor(R.color.red));
        }else{
            txView.setTextColor(getResources().getColor(R.color.red));
        }

    }

    @Override
    public void setEmailEmptyError() {
        EditTextValidadeUtils.setErrorToView(edtEmail, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(true);
        edtLayoutEmail.setError(getString(R.string.error_empty_email));
    }

    @Override
    public void setEmailFormatError() {
        EditTextValidadeUtils.setErrorToView(edtEmail, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(true);
        edtLayoutEmail.setError(getString(R.string.error_format_email));
    }

    @Override
    public void setPasswordEmptyError() {
        EditTextValidadeUtils.setErrorToView(edtPass, getApplicationContext());
        edtLayoutPass.setErrorEnabled(true);
        edtLayoutPass.setError(getString(R.string.error_empty_pass));
    }

    @Override
    public void setConfPasswordEmptyError() {
        EditTextValidadeUtils.setErrorToView(edtConfPass, getApplicationContext());
        edtLayoutConfPass.setErrorEnabled(true);
        edtLayoutConfPass.setError(getString(R.string.error_empty_conf_pass));
    }

    @Override
    public void setPasswordShortError() {
        EditTextValidadeUtils.setErrorToView(edtPass, getApplicationContext());
        edtLayoutPass.setErrorEnabled(true);
        edtLayoutPass.setError(getString(R.string.error_short_pass));
    }

    @Override
    public void setConfPasswordNotEqualsToPassError() {
        EditTextValidadeUtils.setErrorToView(edtPass, getApplicationContext());
        EditTextValidadeUtils.setErrorToView(edtConfPass, getApplicationContext());
        edtLayoutConfPass.setErrorEnabled(true);
        edtLayoutConfPass.setError(getString(R.string.error_pass_not_equal));
    }

    @Override
    public void setNomeDefaultState() {
        EditTextValidadeUtils.setNormalStateToView(edtName, getApplicationContext());
        edtLayoutName.setErrorEnabled(false);
    }

    @Override
    public void setBirthdateDefaultState() {
        EditTextValidadeUtils.setNormalStateToView(edtBirth, getApplicationContext());
        edtLayoutBirth.setErrorEnabled(false);
    }

    @Override
    public void setGenderDefaultState() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            imgSpinner.getDrawable().setColorFilter(getColor(R.color.orange), PorterDuff.Mode.SRC_ATOP);
        else
            imgSpinner.getDrawable().setColorFilter(getResources().getColor(R.color.orange), PorterDuff.Mode.SRC_ATOP);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            containerSpinner.setBackground(getDrawable(R.drawable.view_spinner_border_orange));
        }else{
            containerSpinner.setBackground(getResources().getDrawable(R.drawable.view_spinner_border_orange));
        }
    }

    @Override
    public void setEmailDefaultState() {
        EditTextValidadeUtils.setNormalStateToView(edtEmail, getApplicationContext());
        edtLayoutEmail.setErrorEnabled(false);
    }

    @Override
    public void setPasswordDefaultState() {
        EditTextValidadeUtils.setNormalStateToView(edtPass, getApplicationContext());
        edtLayoutPass.setErrorEnabled(false);
    }

    @Override
    public void setConfPasswordDefaultState() {
        EditTextValidadeUtils.setNormalStateToView(edtConfPass, getApplicationContext());
        edtLayoutConfPass.setErrorEnabled(false);
    }

    @Override
    public void showSuccessDialog() {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setDialogType(ApiDialogTypes.GENERIC_DIALOG);

        String title = getContext().getString(R.string.dialog_title_new_account);

        title = StringUtils.setValue(title, client.getName());

        apiResponse.getDialogType().setTitle(title);
        apiResponse.getDialogType().setMsg(getContext().getString(R.string.dialog_msg_update_account));
        apiResponse.getDialogType().setGobackFunction(true);

        CustomDialog customDialog = new CustomDialog(this, apiResponse, presenter);
        customDialog.show();
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showDialogError(ApiResponse error) {
        super.showDialogError(error, this, this.presenter);
    }

}
