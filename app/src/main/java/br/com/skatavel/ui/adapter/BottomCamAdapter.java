package br.com.skatavel.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.skatavel.R;
import br.com.skatavel.ui.view.activity.InfoNewSpotActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * © Copyright 2017 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class BottomCamAdapter extends RecyclerView.Adapter<BottomCamAdapter.ViewHolder>{

    private Menu        menu;
    private Activity    context;

    public BottomCamAdapter(Menu menu, Activity context) {
        this.menu       = menu;
        this.context    = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.bottom_pic_row, parent, false);
        return new ViewHolder(itemLayoutView, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        MenuItem item = menu.getItem(position);

        if(item != null && item.isVisible()){

            holder.imageView.setImageDrawable(item.getIcon());
            holder.item.setText(item.getTitle());
            holder.setMenuItem(item);
        }

    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @Bind(R.id.imgItem) ImageView   imageView;
        @Bind(R.id.item)    TextView    item;

        MenuItem menuItem;
        Activity activity;
        public ViewHolder(View itemView, Activity activity) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            this.activity = activity;
        }

        public void setMenuItem(MenuItem item){
            this.menuItem = item;
        }

        @Override
        public void onClick(View view) {
            if(menuItem.getItemId() == R.id.action_take_pic){
                ((InfoNewSpotActivity) activity).openCam();
            }else{
                ((InfoNewSpotActivity) activity).openGalery();
            }
        }
    }

}
