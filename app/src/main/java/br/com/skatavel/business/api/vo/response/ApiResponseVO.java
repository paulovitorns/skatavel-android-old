package br.com.skatavel.business.api.vo.response;

import com.google.gson.annotations.SerializedName;

/**
 * © Copyright 2016 SKatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ApiResponseVO {
    @SerializedName("code") public int      status;
    @SerializedName("msg")  public String   message;
}
