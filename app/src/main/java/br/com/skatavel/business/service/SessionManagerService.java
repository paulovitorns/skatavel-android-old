package br.com.skatavel.business.service;

import br.com.skatavel.model.Client;
import br.com.skatavel.model.Session;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface SessionManagerService {

    void createNewSession(Client client);

    Session getCurrentSession();

    void updateCurrentSession(Client client);

    void destroyCurrentSession();
}
