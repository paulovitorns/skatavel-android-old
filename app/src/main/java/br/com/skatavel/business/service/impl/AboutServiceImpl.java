package br.com.skatavel.business.service.impl;

import java.util.Calendar;

import br.com.skatavel.BuildConfig;
import br.com.skatavel.business.service.AboutService;
import br.com.skatavel.common.AboutResultListener;
import br.com.skatavel.util.DateUtils;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class AboutServiceImpl implements AboutService {

    @Override
    public void getConfigInfos(AboutResultListener listener) {

        Calendar calendar   = Calendar.getInstance();
        listener.onSuccess(BuildConfig.VERSION_NAME, DateUtils.getYearFromDate(calendar.getTime()));
    }

}
