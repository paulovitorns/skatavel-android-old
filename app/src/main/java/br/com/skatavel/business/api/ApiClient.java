package br.com.skatavel.business.api;

import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import br.com.skatavel.BuildConfig;
import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import br.com.skatavel.util.MockUtils;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ApiClient extends OkHttpClient {

    private static ApiClient instance;
    private final static long TIMEOUT = Skatavel.getContext()
            .getResources().getInteger(R.integer.default_api_timeout);

    private ApiClient(){};

    public static ApiClient getInstance(){

        if (instance == null){
            instance = new ApiClient();

            if (BuildConfig.DEBUG) {
                instance.interceptors().add(getLogInterceptor());
            }
            if (BuildConfig.BUILD_TYPE.toLowerCase().equals("mock")) {
                instance.interceptors().add(getMockInterceptor());
            }
            instance.setConnectTimeout(TIMEOUT, TimeUnit.SECONDS);
            instance.setReadTimeout(TIMEOUT, TimeUnit.SECONDS);
            instance.setWriteTimeout(TIMEOUT, TimeUnit.SECONDS);
        }
        return instance;
    }

    private static HttpLoggingInterceptor getLogInterceptor(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
                new HttpLoggingInterceptor.Logger() {
                    @Override public void log(String message) {
                        Log.d("OkHttp", message);
                    }
                });
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    private static Interceptor getMockInterceptor() {

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                try {
                    String responseFile         = MockUtils.replaceFromService(chain.request().url().getPath());
                    String responseString       = MockUtils.getStringFromFile(Skatavel.getContext(), responseFile);

                    return new Response.Builder()
                            .code(200)
                            .message(responseString)
                            .request(chain.request())
                            .protocol(Protocol.HTTP_1_0)
                            .body(ResponseBody.create(MediaType.parse("application/json"), responseString.getBytes()))
                            .addHeader("content-type", "application/json")
                            .build();

                } catch (Exception e) {
                    e.printStackTrace();
                    return chain.proceed(chain.request());
                }
            }
        };
    }

    public static String getStringApi(String api){

        String[] arrStr = api.split("\\?")[0].split("/");
        String stringApi = "";

        int indexApi = 0;
        for(int i =0; i < arrStr.length; i++){
            if(arrStr[i].equals("api")){
                indexApi = i;
            }
        }
        stringApi+="/"+arrStr[indexApi]+"/"+arrStr[indexApi+1];

        return stringApi.trim();
    }

}
