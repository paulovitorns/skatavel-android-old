package br.com.skatavel.business.service.impl;

import br.com.skatavel.business.service.ApiErrorResponse;
import br.com.skatavel.common.BaseCommonListener;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.util.NetworkUtils;
import retrofit.Response;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ApiErrorResponseImpl implements ApiErrorResponse {

    @Override
    public void processErrorResponse(Response response, BaseCommonListener listener) {

        ApiResponse apiResponse = null;

        switch (response.code()){
            case 400:
                break;
            default:
                apiResponse = ApiResponse.getDefaultServerError();
                break;
        }

        listener.onError(apiResponse);
    }

    @Override
    public void processFailure(BaseCommonListener listener) {
        if(!NetworkUtils.hasActiveInternetConnection()){
            listener.onConnectionFail(ApiResponse.getDefaultConnectionError());
        }
        else{
            listener.onServerNotRespond(ApiResponse.getDefaultServerError());
        }
    }
}
