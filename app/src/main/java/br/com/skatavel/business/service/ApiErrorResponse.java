package br.com.skatavel.business.service;

import br.com.skatavel.common.BaseCommonListener;
import retrofit.Response;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface ApiErrorResponse {

    void processErrorResponse(Response response, BaseCommonListener listener);

    void processFailure(BaseCommonListener listener);
}
