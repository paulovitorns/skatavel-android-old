package br.com.skatavel.business.service;

import br.com.skatavel.common.AboutResultListener;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface AboutService {
    void getConfigInfos(AboutResultListener listener);
}
