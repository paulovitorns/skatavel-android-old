package br.com.skatavel.business.api.vo.request;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ClientRequest {
    public long     id;
    public String   username;
    public String   password;
    public String   name;
    public String   birthdate;
    public String   gender;
}
