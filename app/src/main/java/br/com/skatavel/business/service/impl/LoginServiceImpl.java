package br.com.skatavel.business.service.impl;

import android.util.Log;

import br.com.skatavel.business.api.Api;
import br.com.skatavel.business.api.vo.request.ClientRequest;
import br.com.skatavel.business.api.vo.response.ClientResponse;
import br.com.skatavel.business.service.LoginService;
import br.com.skatavel.common.LoginResultListener;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.Client;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class LoginServiceImpl implements LoginService {

    ApiErrorResponseImpl apiErrorService = new ApiErrorResponseImpl();

    @Override
    public void loginClient(ClientRequest request, final LoginResultListener listener) {

        Call<ClientResponse> call = Api.getAdapter().clientLogin(request.username, request.password);

        call.enqueue(new Callback<ClientResponse>() {
            @Override
            public void onResponse(Response<ClientResponse> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    if(response.body().apiresponse.status == 200 )
                        listener.onSuccess(new Client(response.body()));
                    else
                        listener.onError(new ApiResponse(response.body().apiresponse));
                }else{
                    Log.e("LOGIN_ERROR","Response Error 4xx/5xx");
                    apiErrorService.processFailure(listener);
                }

            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("LOGIN_ERROR","Fail on Login Request");
                Log.e("LOGIN_ERROR","Fail on Login Request "+t.getMessage());
                Log.e("LOGIN_ERROR","Fail on Login Request "+t.getCause());
                Log.e("LOGIN_ERROR","Fail on Login Request "+t.getStackTrace());
                apiErrorService.processFailure(listener);
            }
        });

    }
}
