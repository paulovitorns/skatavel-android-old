package br.com.skatavel.business.api.vo.response;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
/**
 * © Copyright 2016 SKatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ClientResponse {
    @SerializedName("id")               public long             id;
    @SerializedName("nome")             public String           name;
    @SerializedName("data_nascimento")  public Date             birthdate;
    @SerializedName("genero")           public String           gender;
    @SerializedName("username")         public String           email;
    @SerializedName("response")         public ApiResponseVO    apiresponse;
}
