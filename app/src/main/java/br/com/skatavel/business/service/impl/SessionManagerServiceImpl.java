package br.com.skatavel.business.service.impl;

import br.com.skatavel.business.service.SessionManagerService;
import br.com.skatavel.model.Client;
import br.com.skatavel.model.Session;
import br.com.skatavel.util.SharedPreferencesUtils;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class SessionManagerServiceImpl implements SessionManagerService {

    @Override
    public void createNewSession(Client client) {

        if(SharedPreferencesUtils.getSessionData() == null)
        {
            SharedPreferencesUtils.saveSessionData(new Session(client));
        }else{
            SharedPreferencesUtils.unsetSessionData();
            SharedPreferencesUtils.saveSessionData(new Session(client));
        }

    }

    @Override
    public Session getCurrentSession() {
        return SharedPreferencesUtils.getSessionData();
    }

    @Override
    public void updateCurrentSession(Client client) {
        SharedPreferencesUtils.saveSessionData(new Session(client));
    }

    @Override
    public void destroyCurrentSession() {
        SharedPreferencesUtils.unsetSessionData();
    }
}
