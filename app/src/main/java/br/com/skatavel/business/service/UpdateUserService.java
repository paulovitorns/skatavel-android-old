package br.com.skatavel.business.service;

import br.com.skatavel.business.api.vo.request.ClientRequest;
import br.com.skatavel.common.UpdateClientResultListener;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface UpdateUserService {
    void updateClient(ClientRequest request, UpdateClientResultListener listener);
}
