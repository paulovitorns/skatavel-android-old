package br.com.skatavel.business.api;

import br.com.skatavel.business.api.vo.response.ClientResponse;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public interface ServiceApi {

    @FormUrlEncoded
    @POST("users/api_login")
    Call<ClientResponse> clientLogin(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("api/user/recuperarsenha")
    Call<ClientResponse> resetLogin(@Field("username") String username);

    @FormUrlEncoded
    @POST("users/api_add")
    Call<ClientResponse> registerClient(
            @Field("nome")              String name,
            @Field("data_nascimento")   String birth,
            @Field("genero")            String gender,
            @Field("username")          String email,
            @Field("password")          String password
    );

    @FormUrlEncoded
    @POST("users/api_edit")
    Call<ClientResponse> updateClient(
            @Field("id")                long   id,
            @Field("nome")              String name,
            @Field("data_nascimento")   String birth,
            @Field("genero")            String gender,
            @Field("username")          String email,
            @Field("password")          String password
    );
}
