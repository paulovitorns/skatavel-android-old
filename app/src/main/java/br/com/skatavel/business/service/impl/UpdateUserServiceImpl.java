package br.com.skatavel.business.service.impl;

import android.util.Log;

import br.com.skatavel.business.api.Api;
import br.com.skatavel.business.api.vo.request.ClientRequest;
import br.com.skatavel.business.api.vo.response.ClientResponse;
import br.com.skatavel.business.service.UpdateUserService;
import br.com.skatavel.common.UpdateClientResultListener;
import br.com.skatavel.model.ApiResponse;
import br.com.skatavel.model.Client;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class UpdateUserServiceImpl implements UpdateUserService {

    ApiErrorResponseImpl apiErrorService = new ApiErrorResponseImpl();

    @Override
    public void updateClient(ClientRequest request, final UpdateClientResultListener listener) {

        Call<ClientResponse> call = Api.getAdapter().updateClient(
                request.id,
                request.name,
                request.birthdate,
                request.gender,
                request.username,
                request.password
        );

        call.enqueue(new Callback<ClientResponse>() {
            @Override
            public void onResponse(Response<ClientResponse> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    if(response.body().apiresponse.status == 200 )
                        listener.onSuccess();
                    else
                        listener.onError(new ApiResponse(response.body().apiresponse));
                }else{
                    Log.e("LOGIN_ERROR","Response Error 4xx/5xx");
                    apiErrorService.processFailure(listener);
                }

            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("LOGIN_ERROR","Fail on Update CLiente Request");
                Log.e("LOGIN_ERROR","Fail on Update CLiente Request "+t.getMessage());
                Log.e("LOGIN_ERROR","Fail on Update CLiente Request "+t.getCause());
                Log.e("LOGIN_ERROR","Fail on Update CLiente Request "+t.getStackTrace());
                apiErrorService.processFailure(listener);
            }
        });

    }
}
