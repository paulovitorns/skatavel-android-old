package br.com.skatavel.business.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.HttpUrl;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class Api {

    private static ServiceApi adapter;

    public static final String url =  Skatavel.getContext().getString(R.string.url_api);
    public static final HttpUrl PRODUCTION_API_URL = HttpUrl.parse(url);

    private Api() {}

    private static ServiceApi createRestAdapter(){

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .create();

        System.setProperty("http.keepAlive", "false");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PRODUCTION_API_URL)
                .client(ApiClient.getInstance())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(ServiceApi.class);
    }

    public static ServiceApi getAdapter(){
        if (adapter == null)
            adapter = createRestAdapter();
        return adapter;
    }

}
