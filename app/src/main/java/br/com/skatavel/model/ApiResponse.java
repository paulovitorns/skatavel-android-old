package br.com.skatavel.model;

import br.com.skatavel.business.api.vo.response.ApiResponseVO;

/**
 * © Copyright 2016 SKatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ApiResponse {

    private int             code;
    private String          message;
    private ApiDialogTypes dialogType;

    public ApiResponse(){

    }

    public static ApiResponse getDefaultServerError(){
        ApiResponse api = new ApiResponse();
        api.dialogType = ApiDialogTypes.ERROR_TYPE_SERVER;

        return api;
    }

    public static ApiResponse getDefaultConnectionError(){
        ApiResponse api = new ApiResponse();
        api.dialogType = ApiDialogTypes.ERROR_TYPE_NO_CONNECTION;

        return api;
    }

    public static ApiResponse getDefaultSessionNeeded(){
        ApiResponse api = new ApiResponse();
        api.dialogType = ApiDialogTypes.NEED_LOGIN;

        return api;
    }

    public ApiResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApiResponse(ApiResponseVO responseVO) {
        this.code       = responseVO.status;
        this.message    = responseVO.message;

        parseErrorType();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ApiDialogTypes getDialogType() {
        return dialogType;
    }

    public void setDialogType(ApiDialogTypes dialogType) {
        this.dialogType = dialogType;
    }

    private void parseErrorType(){

        switch (this.code){
            case 409:

                if(getMessage().equalsIgnoreCase("Login ou Senha incorretos"))
                    this.dialogType = ApiDialogTypes.ERROR_TYPE_LOGIN_INCORRECT;
                else
                    this.dialogType = ApiDialogTypes.ERROR_TYPE_EMAIL_REGISTERED;

                break;
            default:
                this.dialogType = ApiDialogTypes.ERROR_TYPE_SERVER;
        }

    }
}
