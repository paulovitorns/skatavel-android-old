package br.com.skatavel.model;

import java.io.Serializable;
import java.util.Date;

import br.com.skatavel.business.api.vo.response.ClientResponse;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class Client implements Serializable {

    public static String KEY = Client.class.getSimpleName();

    private long    id;
    private String  name;
    private Date    birthdate;
    private String  gender;
    private String  email;
    private String  password;

    public Client() {
    }

    public Client(String name, Date birthdate, String gender, String email, String password) {
        this.name       = name;
        this.birthdate  = birthdate;
        this.gender     = gender;
        this.email      = email;
        this.password   = password;
    }

    public Client(long id, String name, Date birthdate, String gender, String email, String password) {
        this.id         = id;
        this.name       = name;
        this.birthdate  = birthdate;
        this.gender     = gender;
        this.email      = email;
        this.password   = password;
    }

    public Client(ClientResponse response) {
        this.id         = response.id;
        this.name       = response.name;
        this.birthdate  = response.birthdate;
        this.gender     = response.gender;
        this.email      = response.email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
