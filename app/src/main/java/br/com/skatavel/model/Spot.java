package br.com.skatavel.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class Spot implements Serializable {

    public static String KEY = Spot.class.getSimpleName();

    private long    id;
    private LatLng  latLng;
    private String  name;
    private String  infos;

    public Spot(long id, LatLng latLng, String name, String infos) {
        this.id     = id;
        this.latLng = latLng;
        this.name   = name;
        this.infos  = infos;
    }

    public Spot(LatLng latLng, String name, String infos) {
        this.latLng = latLng;
        this.name   = name;
        this.infos  = infos;
    }

    public Spot(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }
}
