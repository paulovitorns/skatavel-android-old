package br.com.skatavel.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class ListOfSpots implements Serializable {

    public static String KEY = ListOfSpots.class.getSimpleName();

    private List<Spot> spotList;

    public ListOfSpots() {
        this.spotList = new ArrayList<>();
    }

    public List<Spot> getSpotList() {
        return spotList;
    }

    public void setSpotList(List<Spot> spotList) {
        this.spotList = spotList;
    }

    public void addSpot(Spot spot){
        spotList.add(spot);
    }

    public void removeBySpot(Spot spot){
        spotList.remove(spot);
    }

    public void removeByPositioSpot(int position){
        spotList.remove(position);
    }
}
