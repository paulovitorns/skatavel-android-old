package br.com.skatavel.model;

import android.support.v4.app.Fragment;

import br.com.skatavel.ui.view.fragment.FavoriteFragment;
import br.com.skatavel.ui.view.fragment.MapSearchFragment;
import br.com.skatavel.ui.view.fragment.ProfileFragment;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public enum DashboardNavigation {
    PROFILE_NAV("PROFILE", ProfileFragment.newInstance()),
    MAPS_NAV("MAPS", MapSearchFragment.newInstance()),
    FAVORITE_NAV("FAVORITES", FavoriteFragment.newInstance());

    private String      titlePage;
    private Fragment    fragment;

    DashboardNavigation(String titlePage, Fragment fragment) {
        this.titlePage  = titlePage;
        this.fragment   = fragment;
    }

    public String getTitlePage() {
        return titlePage;
    }

    public void setTitlePage(String titlePage) {
        this.titlePage = titlePage;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }
}
