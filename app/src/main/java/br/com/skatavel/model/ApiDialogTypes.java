package br.com.skatavel.model;

import android.content.Intent;
import android.provider.Settings;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;

/**
 * © Copyright 2016 SKatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public enum ApiDialogTypes {

    ERROR_TYPE_SERVER(
            Skatavel.getContext().getString(R.string.error_server_title),
            Skatavel.getContext().getString(R.string.error_server_msg),
            true,
            false,
            false,
            null
    ),
    ERROR_TYPE_NO_CONNECTION(
            Skatavel.getContext().getString(R.string.error_not_connection_title),
            Skatavel.getContext().getString(R.string.error_not_connection_msg),
            true,
            false,
            false,
            null
    ),
    ERROR_TYPE_SERVER_NOT_RESPONDE(
            Skatavel.getContext().getString(R.string.error_server_not_responde_title),
            Skatavel.getContext().getString(R.string.error_server_not_responde_msg),
            true,
            false,
            false,
            null
    ),
    ERROR_TYPE_LOGIN_INCORRECT(
            Skatavel.getContext().getString(R.string.error_login_incorrect_title),
            Skatavel.getContext().getString(R.string.error_login_incorrect_msg),
            false,
            false,
            false,
            null
    ),
    ERROR_TYPE_EMAIL_REGISTERED(
            Skatavel.getContext().getString(R.string.error_email_registered_title),
            Skatavel.getContext().getString(R.string.error_email_registered_msg),
            false,
            true,
            false,
            null
    ),
    GENERIC_DIALOG(
            null,
            null,
            false,
            false,
            false,
            null
    ),
    NETWORK_DISABLED_SNACKBAR_DIALOG(
            Skatavel.getContext().getString(R.string.error_not_connection_action_title),
            Skatavel.getContext().getString(R.string.error_not_connection_msg_snackbar),
            false,
            false,
            false,
            new Intent(Settings.ACTION_WIFI_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
    ),
    LOCATION_DISABLED_SNACKBAR_DIALOG(
            Skatavel.getContext().getString(R.string.error_not_location_action_title),
            Skatavel.getContext().getString(R.string.error_not_location_msg_snackbar),
            false,
            false,
            false,
            new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
    ),
    NEED_LOGIN(
            Skatavel.getContext().getString(R.string.error_not_logged_title),
            Skatavel.getContext().getString(R.string.error_not_logged_msg),
            false,
            false,
            false,
            null
    );

    private String  title;
    private String  msg;
    private boolean showTryAgainButton;
    private boolean showResetPassButton;
    private boolean gobackFunction;
    private Intent  intentExtras;

    ApiDialogTypes(String title, String msg, boolean showTryAgainButton, boolean showResetPassButton, boolean gobackFunction, Intent intentExtras){
        this.title                  = title;
        this.msg                    = msg;
        this.showTryAgainButton     = showTryAgainButton;
        this.showResetPassButton    = showResetPassButton;
        this.gobackFunction         = gobackFunction;
        this.intentExtras           = intentExtras;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isShowResetPassButton() {
        return showResetPassButton;
    }

    public void setShowResetPassButton(boolean showResetPassButton) {
        this.showResetPassButton = showResetPassButton;
    }

    public boolean isShowTryAgainButton() {
        return showTryAgainButton;
    }

    public void setShowTryAgainButton(boolean showTryAgainButton) {
        this.showTryAgainButton = showTryAgainButton;
    }

    public boolean isGobackFunction() {
        return gobackFunction;
    }

    public void setGobackFunction(boolean gobackFunction) {
        this.gobackFunction = gobackFunction;
    }

    public Intent getIntentExtras() {
        return intentExtras;
    }

    public void setIntentExtras(Intent intentExtras) {
        this.intentExtras = intentExtras;
    }
}
