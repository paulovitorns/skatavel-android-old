package br.com.skatavel.model;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import br.com.skatavel.R;
import br.com.skatavel.Skatavel;
import br.com.skatavel.ui.view.activity.AboutActivity;
import br.com.skatavel.ui.view.activity.LoginActivity;
import br.com.skatavel.ui.view.activity.RegisterActivity;
import br.com.skatavel.ui.view.activity.UpdateUserActivity;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public enum ProfileMenu {

    MY_GAPS(Skatavel.getContext().getString(R.string.profile_my_gaps), false, false, R.style.CommonLineBold_FontRegular, false, false),
    INFOS_APP(Skatavel.getContext().getString(R.string.profile_about), true, true, 0, false, false),
    SHARE_APP(Skatavel.getContext().getString(R.string.profile_share), true, true, 0, false, true),
    ACCESS_ACCOUNT(Skatavel.getContext().getString(R.string.profile_login), true, false, R.style.CommonLineBold_FontRegular, false, false),
    REGISTER_USER(Skatavel.getContext().getString(R.string.profile_register), true, false, R.style.CommonLineBold_FontRegular, false, false),
    UPDATE_ACCOUNT(Skatavel.getContext().getString(R.string.profile_update), false, false, R.style.CommonLineBold_FontRegular, false, false),
    LOGOUT(Skatavel.getContext().getString(R.string.profile_logout), false, false, R.style.CommonLineBold_FontRegular, true, false);

    private String  menu;
    private boolean isPublic;
    private boolean isDefaultFontStyle;
    public boolean  isLogOutFunction;
    public boolean  isShareFunction;
    private int     fontStyle;
    private Intent  navigation;

    ProfileMenu(String menu, boolean isPublic, boolean isDefaultFontStyle, int fontStyle, boolean isLogOutFunction, boolean isShareFunction) {
        this.menu               = menu;
        this.isPublic           = isPublic;
        this.isDefaultFontStyle = isDefaultFontStyle;
        this.fontStyle          = fontStyle;
        this.isLogOutFunction   = isLogOutFunction;
        this.isShareFunction    = isShareFunction;
    }

    public void setNavigation(Context context, ProfileMenu profileMenu){
        switch (profileMenu){
            case MY_GAPS:
                navigation = null;
                break;
            case INFOS_APP:
                navigation = new Intent(context, AboutActivity.class);
                break;
            case ACCESS_ACCOUNT:
                navigation = new Intent(context, LoginActivity.class);
                break;
            case REGISTER_USER:
                navigation = new Intent(context, RegisterActivity.class);
                break;
            case UPDATE_ACCOUNT:
                navigation = new Intent(context, UpdateUserActivity.class);
                break;
            default:
                navigation = null;
                break;
        }
    }

    public static List<ProfileMenu> getPublicItens(){
        List<ProfileMenu> itensPub = new ArrayList<>();

        for (int i = 0; i < ProfileMenu.values().length; i++){
            ProfileMenu menu = ProfileMenu.values()[i];
            if(menu.isPublic()) {
                itensPub.add(ProfileMenu.values()[i]);
            }
        }

        return  itensPub;
    }

    public static List<ProfileMenu> getPrivateItens(){
        List<ProfileMenu> itensPub = new ArrayList<>();

        for (int i = 0; i < ProfileMenu.values().length; i++){
            ProfileMenu menu = ProfileMenu.values()[i];
            if(menu != ACCESS_ACCOUNT && menu != REGISTER_USER)
                itensPub.add(ProfileMenu.values()[i]);
        }

        return  itensPub;
    }

    public String getMenu() {
        return menu;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public boolean isDefaultFontStyle() {
        return isDefaultFontStyle;
    }

    public boolean isLogOutFunction() {
        return isLogOutFunction;
    }

    public boolean isShareFunction() {
        return isShareFunction;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public Intent getNavigation() {
        return navigation;
    }

    public void navigation(Context context){
        context.startActivity(navigation);
    }

}
