package br.com.skatavel.model;

import java.io.Serializable;

/**
 * © Copyright 2016 Skatavel.
 * Autor : Paulo Sales - dev@paulovns.com.br
 * Empresa : Skatavel app.
 */

public class Session implements Serializable {

    public static final String KEY = Session.class.getSimpleName();

    private Client client;

    public Session(Client client) {
        this.client = client;
    }

    public void setSession(Client client){
        this.client = client;
    }

    public Client getClient(){
        return this.client;
    }

    public void cleanSession(){
        this.client = null;
    }

    public void updateClient(Client client){
        this.client = client;
    }

}
